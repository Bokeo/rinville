Genus Arthrobacter was first proposed by Conn and Dimmick () with the description of the type species Arthrobacter globiformis. Subsequently, Koch et al. () emended the description with the reclassification of Micrococcusagilis as Arthrobacteragilis. The currently validated 78 members of genus Arthrobacter are members of phylum Actinobacteria, order Actinomycetales, family Micrococcaceae, and are characterized by the presence of a rod–coccus growth cycle and genomes with high G + C content (59–66 mol%) (Keddie et al. ). Members of genus Arthrobacter stain Gram positive are catalase positive and are sub-divided on the basis of the lysine-containing peptidoglycan into two groups, A3α and A4α (Schleifer and Kandler ; Stackebrandt et al. ; Keddie et al. ; Koch et al. ).

The primary habitat of Arthrobacter is soil and interestingly Arthrobacter strains with the ability to degrade nitrophenols and/or chlorophenols which include Arthrobacter chlorophonolicus A6 (Westerberg et al. ), Arthrobacter ureafaciens, strain CPR706 (Bae et al. ), Arthrobacter citrus (Karigar et al. ), Arthrobacter protophormae strain RKJ100 (Chauhan et al. ), Arthrobacter sp. strain JS443 (Jain et al. ) and Arthrobacter aurescens TW17 (Hanne et al. ) have all been isolated from pesticide-contaminated soil. We had previously reported for the first time on the isolation of Arthrobacter strain SJCon that degraded 2-chloro-4-nitrophenol (2C4NP) (Arora and Jain ). Chlorohydroquinone was identified as a major intermediate product which was further degraded via formation of maleylacetate (Arora and Jain ). As strain SJConT is a potential degrader of various nitrophenolic compounds including 2-chloro-4-nitrophenol, 4-nitrophenol and 3-methyl-4-nitrophenol, and it has the potential for use in the bioremediation of nitrophenolic contaminated sites. In this communication, we report on the chemotaxonomic and genotypic properties of the strain and designate it as a new species of genus, A. nitrophenolicus sp nov. (The type strain is SJConT =MTCC 10104T =DSM 23165T).

The method for isolating strain SJConT from a pesticide-contaminated soil by an enrichment method using 2-chloro-4-nitrophenol as sole carbon source (Sigma-Aldrich, GmbH, Steinheim, Germany) has been reported previously (Arora and Jain ).

Colony morphology was examined on Nutrient agar plates after incubation at 30 °C for 24 h. Cell morphology was examined by light microscopy (Zeiss) at ×1000 and motility was checked using the method described by Skerman (). Gram staining was performed using HiMedia Gram Staining kit (HiMedia, India) according to the manufacturer’s instructions. Strain SJConT had morphological characteristics consistent with members of the genus Arthrobacter. Cells exhibited a rod–coccus growth cycle, were non-motile, did not form spores and stained Gram positive.

Growth at different temperatures (between 4 and 50 °C) was determined using Nutrient agar plates. Strain SJConT was streaked on nutrient agar plates, incubated at different temperatures and the growth on the plate scored. The growth in nutrient broth containing different NaCl concentrations (0.5–7 %) was monitored by measuring optical density at 600 nm in a Lambda 35 spectrophotometer (Perkin Elmer). Growth at different pH (pH 4.5–12) was tested in nutrient broth after the pH had been adjusted using appropriate buffers as described previously (Arora et al. ). Growth occurred between 10 and 40 °C with the optimum temperature for growth at 30 °C. The optimum pH for growth was 7 and no growth occurred below pH 6 or above pH 10. NaCl was not required for growth, but was tolerated up to 4 %.

Hydrolysis of gelatin, casein, starch, Voges-Proskauer, methyl red, oxidation-fermentation tests, catalase and oxidase activities, growth on Simmon’s citrate and MacConkey agar, production of H2S and indole, reduction of nitrate and acid production from carbohydrates were determined as described previously (Arora et al. ).

A summary of the results from the phenotypic tests is presented in Table  and listed in the species description.Differentiating characteristics of strain Arthrobacter nitrophenolicus strain SJConT and Arthrobacter globiformis DSM 20124T

Characteristics features	Arthrobacter nitrophenolicus	Arthrobacter globiformis	
Type strains	SJConT =MTCC 10104T =DSM 23165T	DSM 20124T	
Sample source	Pesticide-contaminated soil	Soil	
16S rRNA gene accession number	GQ927310	M23411	
Mol% G + C	69 ± 1 mol%	62.0 %	
DNA homology (%) of strain SJConT with:	100	45	
NaCl range	0–4 %	0–5 %	
Starch hydrolysis	–	+	
Casein hydrolysis	+	–	
Oxidation of substrates	
 d-Mannitol	+	–	
Utilization of various nitro aromatics as sole carbon and energy source	
 2-Chloro-4-nitrophenol	+	–	
 4-Nitrophenol	+	–	
 3-Methyl-4-nitrophenol	+	–	
Both species produce white colonies, have a rod-coccus life cycle, grow between pH 6 and 10, do not reduce nitrate, hydrolyse gelatin and utilise l-arabinose and glucose



The utilization of carbon and energy sources was tested using Biolog GP2 Microplates (Hayward, CA). For this, the inoculum was prepared by re-suspending Nutrient agar grown colonies to a turbidity equivalent to 0·5 McFarland units and the GP2 Microplates inoculated following the manufacturer’s instructions. The plates were incubated at 30 °C for 24 h and the results read using a MicroPlate Reader equipped with Microlog 4.2 software. The results are listed in the species description.

Menaquinones, fatty acids, polar lipids and peptidoglycan were analyzed by standard methods (Arora et al. ). The major menaquinone was identified as MK-9(H2), a characteristic chemotaxonomic marker of the genus Arthrobacter. The fatty acid profile comprises C16:0 (6.48 %), C18:0 (1.61 %), iso-C14:0 (0.93 %), iso-C15:0 (16.33 %), anteiso-C15:0 (44.18 %), iso-C16:0 (9.25 %), iso-C17:0 (5.61 %) and anteiso-C17:0 (15.19 %). The major polar lipids were diphosphatidylglycerol (DPG), phosphatidylglycerol (PG), phosphatidylinositol (PI) and a glycolipid. The peptidoglycan was identified as type A3α (l-Lys–l-Ala3).

DNA extraction and purification have been described previously (Arora and Jain ). The 16S rRNA gene was amplified using universal bacterial primers 8F (5′-AGA GTT TGA TCC TGG CTC AG-3′) and 1492R (5′-GGT TAC CTT GTT ACG ACT T-3′) (Arora et al. ) and sequenced using n ABI automated sequencer (Applied Biosystems, USA). The sequence (1,446 bp with GenBank accession number GQ927310) was compared against sequences available in the GenBank database (version 188.0) using EzTaxon server 2.1, the sequences of the nearest phylogentic members downloaded, aligned with those of related Arthrobacter species and a phylogenetic tree constructed using the neighbour-joining method as implemented in MEGA (Tamura et al. ) and Bioedit (Hall ). During phylogenetic reconstruction, all ambiguous nucleotides were excluded from the analysis and a total of 1,270 nucleotides were used in the final analysis. Phylogenetic analysis showed that strain SJConT was a member of the genus Arthrobacter and showed the highest similarity to A. globiformis DSM 20124T and related members of the ‘globiformis’ group (average sequence similarity of 97%) (Fig. ).The phylogenetic position of Arthrobacter nitrophenolicus strain SJConT within the radiation of members of genus Arthrobacter based on 16S rRNA gene. The type strains are indicated with the letterT and the accession numbers are shown in brackets. Bootstrap values &gt;95 % (expressed as percentages of 1000 replications) are indicated at the branch points



The G + C mol% content of the genomic DNA was determined in a Lambda 35 spectrophotometer (Perkin Elmer, Waltham, MA, USA) using the thermal denaturation (Tm) method and determined to be 69 ± 1 mol%.

DNA hybridization was performed using Biotin DecaLabelTM Kit and Biotin Chromogenic Detection Kit (Fermentas Life Sciences) following the manufacturer’s instruction. The DNA–DNA relatedness value of strain SJConT with A. globiformis DSM 20124T was found to be 45 %.

Strain SJConT degrades 2-chloro-4-nitrophenol, 4-nitrophenol and 3-methyl-4-nitrophenol. It possesses morphological characteristic of rod-coccus life cycle, which is consistent with that reported for members of the genus Arthrobacter. It’s affiliation to this genus is supported by the chemotaxonomic traits and 16S rRNA phylogeny. In addition, the presence of cell-wall peptidoglycan type A3α and menaquinone MK-9(H2) supports the placement of strain SJConT within the “globiformis” group. However, the large phylogenetic distance, the low DNA homology and the higher mol % G + C content and a number of phenotypic traits (Table ) differentiate strain SJConT from A. globiformis DSM 20124T and members of the “globiformis” group. The fatty acid composition of strain SJconT was differed from the fatty acid composition of A. globiformis DSM 20124T (Table ). The fatty acid profile of strain SJConT was comprises C16:0 (6.48 %), C18:0 (1.61 %), iso-C14:0 (0.93 %), iso-C15:0 (16.33 %), anteiso-C15:0 (44.18 %), iso-C16:0 (9.25 %), iso-C17:0 (5.61 %) and anteiso-C17:0 (15.19 %). On the basis of these polyphasic differences, strain SJConT represents a novel species of the genus Arthrobacter, for which the name Arthrobacter nitrophenolicus sp. nov. is proposed. The type strain is SJConT (=MTCC 10104T =DSM 23165T).Fatty acid profile of Arthrobacter nitrophenolicus strain SJConT and Arthrobacter globiformis DSM 20124T (Data from present study)

Fatty acid	Arthrobacter nitrophenolicus	A. globiformis	
14:0 iso	0.93	ND	
15:0 iso	16.33	13.3	
15:0 anteiso	44.18	55.2	
16:0 iso	9.25	21.0	
16:0	6.48	ND	
17:0 iso	5.61	ND	
17:0 anteiso	15.19	10.50	
18:0	1.61	ND	
ND Not detected



Description of Arthrobacter nitrophenolicus sp. nov.
(Nitr.o.phen.o.li.cus.M.L. adj. Nitro containing nitrogen, N.L. n. phenol phenol; M.L. adj. nitrirophenolicus relating to nitrophenol).

Cells are Gram-positive, catalase-positive, oxidase-negative, non-spore-forming, non-motile and exhibit a rod–coccus growth cycle. The optimal temperature for growth is 30 °C (temperature growth range of 10–40 °C) and pH 7.0 (pH growth range of pH 6–10). The cells grow on nutrient broth medium with 4 % NaCl, and degrade 2-chloro-4-nitrophenol, 4-nitrophenol and 3-methyl-4-nitrophenol. Chlorohydroquinone, a major intermediate product of degradation of 2-chloro-4-nitrophenol, which is further degraded via formation of maleylacetate. It hydrolyses gelatin and casein but not starch. No growth occurs on Simmon Citrate and Mac Conkey Agar. Chlorohydroquinone is negative for indole production, H2S production, nitrate reduction, Voges-Proskaur test, methyl red, and oxidation-fermentation tests; It produce acids from l-arabinose, d-glucose, and d-manitol, and oxidizes the following substrates in Biolog GN2 Microplates: arbutin, d-cellobiose, d-fructose, d-galactose, d-gluconic acid, α-d-glucose, m-inositol, α-d-lactose, maltotriose, d-mannitol, d-mannose, d-melizitose, d-ribose, salicin, d-sorbitol, sucrose, turanose, d-xylose, acetic acid, β-hydroxybutyric acid, γ-hydroxybutyric acid, α-ketoglutaric acid, l-lactic acid, l-malic acid, pyruvatic acid methyl ester, pyruvic acid, l-alaninamide, d-alanine, l-alanine, L-alanyl-Glycine, l-asparagine, l-glutamic acid, glycyl-l-glutamic acid, L-pyroglutamic acid, l-serine, putrescine, glycerol, adenosine, inosine. C16:0 (6.48), C18:0 (1.61 %), iso-C14:0 (0.93 %), iso-C15:0 (16.33 %), anteiso-C15:0 (44.18 %), iso-C16:0 (9.25 %), iso-C17:0 (5.61 %) and anteiso-C17:0 (15.19 %) are the major fatty acids. The major polar lipids are diphosphatidylglycerol, phosphatidylglycerol, phosphatidylinositol and a glycolipid. The major menaquinone detected is MK-9(H2) and the peptidoglycan-type is A3α. The DNA G + C content of strain SJConT is 69 ± 1 mol%.

The type strain SJConT =MTCC 10104T =DSM 23165T was isolated from a pesticide-contaminated soil sample collected in Punjab State, India.

Open Access
This article is distributed under the terms of the Creative Commons Attribution License which permits any use, distribution and reproduction in any medium, provided the original author(s) and source are credited.

This article is dedicated in the memory of Dr Rakesh Jain for his excellent contributions to the field of microbial sciences and biodegradation.

