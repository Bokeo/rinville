Introduction
Stevia rebaudiana Bertoni, a member of Asteraceae family, native to certain regions of South America-Brazil and Paraguay (Alhady ), is one of the important anti-diabetic medicinal herbs. It is indigenous to the Rio Monday Valley of the Amambay mountain region at altitudes between 200 and 500 m (Pande and Gupta ). The compounds in its leaves, stevioside and rebaudioside taste about 300 times sweeter than sucrose (Geuns ). It is used as sweetening agent and has enormous commercial importance. Its other medicinal uses include regulating blood sugar, preventing hypertension and tooth decay, and treatment of skin disorders (Singh and Rao ). Stevia also has healing effect on blemishes, wound cuts and scratches, besides being helpful in weight and blood pressure management. Conventional propagation in this plant is restricted due to the poor seed viability coupled with very low germination rate. Role of vegetative propagation method is also limited as specific habitat conditions are mandatory to grow the plants in addition to low acclimatization rate in soil. A suitable alternative method to prepare sufficient amount of plants within short time duration is the use of in vitro cultures. There are reports of in vitro clonal propagation of Stevia using nodal segments (Sivaram and Mukundan ; Mitra and Pal ; Singh et al. ). In vitro clonal propagation of Stevia has been carried out using nodal, inter-nodal segment (Uddin et al. ; Ahmed et al. ; Sairkar et al. ; Thiyagarajan and Venkatachalam ), leaf (Ali et al. ) and shoot-tip explants (Anbazhagan et al. ; Das et al. ). The present study was undertaken to evaluate the effectiveness of two-stage culture procedures as means of micropropagation of S. rebaudiana in half strength MS media, which has not been attempted before, using various cytokinins—benzylaminopurine (BAP), kinetin (Kn) and thidiazuron (TDZ). The study also compares the stevioside content in the in vivo and in vitro leaves, supporting the effectiveness of micropropagation protocol generated.

Materials and methods
Explant source
Healthy plants of S. rebaudiana were collected from local herbal nurseries and established in horticulture garden of BHU campus. Nodal explants (1.5–2.0 cm long and 0.2–0.4 cm thick) were washed thoroughly for 15 min under running tap water, treated with Tween 80 (2–3 drops in 100 ml) for 7 min followed by treatment with 0.002 % (w/v) bavistin for 2–3 min. These were surface sterilized with 0.1 % (w/v) HgCl2 for 2–3 min and washed 4–5 times with sterile double distilled water.

Shoot induction media
Murashige and Skoog medium () (half strength) was used supplemented with 3 % (w/v) sucrose, ascorbic acid (50 mg/l), gibberellic acid (1 mg/l), solidified with 0.8 % (w/v) agar; pH was adjusted to 5.8 prior to autoclaving at 121 °C for 20 min. The nodal explants were inoculated with different concentrations of TDZ (0.01, 0.03, 0.05, 0.1, 0.2, 0.5 mg/l), BAP (0.2, 0.5, 1.0 mg/l) and Kn (0.2, 0.5, 1.0 mg/l) to observe their effect on shoot development and multiplication. The cultures were kept under cool, fluorescent light (16 h photoperiod) at 25 ± 2 °C in the culture room. Data were noted after 3 weeks of inoculation.

Multiplication media
Shoots (3-week-old) obtained from the shoot induction media containing 0.2 mg/l BAP (B), 0.2 mg/l Kn (E), 0.2 mg/l each of BAP and Kn (H), 0.5 mg/l TDZ (P), as shown in Table , were sub-cultured on multiplication media. The multiplication medium consisted of half MS supplemented with TDZ (0.01 mg/l); half MS devoid of any growth regulator and original shoot induction medium (B, E, H and P), as shown in Table ; cultures without growth regulators served as control. Total number of shoots, length of shoots and number of leaves were observed after 4 weeks of culture.


In vitro root induction
The elongated shoots with length more than 5.0 cm were excised from the culture flasks and transferred to the rooting media amended with 0.2, 0.5 and 1.0 mg/l IBA under aseptic condition. The growth regulator was added separately to half strength and one-fourth strength MS media containing 3 % sucrose to determine the effect of MS salt concentration on root induction (Table ). The media was additionally supplemented with activated charcoal (50 mg/l), ascorbic acid (50 mg/l), polyvinylpolypyrrolidone (100 mg/l) and gibberellic acid (0.5 mg/l). Total number of roots per shoot as well as length of the roots was measured after 4 weeks of culture.

Hardening
Rooted plants were carefully removed from the culture flasks, washed with sterile water to remove agar media, placed in the plastic cups filled with sterilized perlite. The plants were covered with polythene bags to maintain high humidity. These plants were maintained in the culture room for 3 weeks with the following atmospheric conditions: temperature, 25 ± 2 °C; light, 16 h photoperiod. Afterwards these were transferred to pots containing sterilized garden soil and sand (1:1).

Observation recorded and statistical analysis
Observations were recorded and different parameters (number of shoots, length of shoots and number of leaves) were examined using 8–10 replicates. Data were subjected to Duncan’s multiple range test (Duncan ).

Stevioside analysis
Biochemical analysis of the in vivo and in vitro-raised plants was performed using more sensitive and rapid method of HPLC (Shimadzu, Japan) analysis to confirm the presence of stevioside in leaves. Stevioside standard was obtained from Sigma Ltd., USA (95 % purity).

Conditions for HPLC study: column used C-18, mobile phase used methanol:water (80:20) with the flow rate of 1.5 ml/min (injection 10 μl). Peak detection was made at 210 nm at the room temp of 25 °C.

Standard preparation: 1 mg of stevioside sample was dissolved in 10 ml of methanol. Then 10 μl was applied to HPLC chromatogram.

Sample preparation from Stevia leaves: Stevia leaves were dried at 50 °C for 24 h in dark and pulverized to uniform size. This material (2 g) was extracted in boiling water (2 × 50 ml) and further boiled for 30 s. Cooled extract was first filtered through the filter paper and then microfiltered (0.45 μm) before it was ready for analysis.

Calculation of percentage of stevioside (X) in the sample was done as per the formula*:where, WS is the amount (mg) of stevioside in the standard solution W is the amount (mg) of sample in the sample solution AS is the peak area for stevioside from the standard solution AX is the peak area of X for the sample solution fX is the ratio of the formula weight of X to the formula weight of stevioside: 1.00 (stevioside).

Prepared at the 68th Joint FAO/WHO Expert Committee on Food Additives JECFA ().

Results and discussion
Shoot induction
The nodal explants were inoculated in half strength MS media with various concentration of BAP (0.2, 0.5, 1.0 mg/l). Almost at every concentration, bud break was seen during 3–7 days after inoculation. The parameters recorded (number of shoots, length of shoot and number of leaves) have shown differences at various concentrations in the induction media. 0.2 mg/l BAP showed better growth in terms of number of shoots (2.25 ± 0.25), shoot length (1.20 ± 0.27 cm), number of leaves (15.75 ± 1.84). Multiple shoot observed from 1.0 to 2.0 mg/l BAP (MS media) and maximum response as well as healthy shoot was noticed at 1 mg/l BAP (Ranganathan ). When kinetin was used, 0.2 mg/l showed better response in terms of all the parameters; number of shoots (2.00 ± 0.00), shoot length (1.95 ± 0.35 cm), though media with 0.5 mg/l of kinetin has shown higher number of leaves (20.00 ± 0.81). BAP and Kinetin at different concentrations (0.2, 0.5, 1.0 mg/l of each) when applied together, produced the best response at lower concentration (0.2 mg/l), in terms of number of shoots (2.00 ± 0.00), shoot length (3.10 ± 0.96 cm) and number of leaves (11.50 ± 0.50). Mehta et al. () reported that best shooting response was observed on MS media containing 0.5 mg/l BAP + 2.0 mg/l Kn (average number of shoots 3.42 ± 0.39) and 0.5 mg/l BAP + 0.5 mg/l Kn (average shoot length 7.54 ± 0.31 cm). BAP (0.2 mg/l alone) and BAP/Kn (0.5 mg/l each) showed same result in terms of number of shoots, but considering all the parameters together, BAP (0.2 mg/l) produced the best response in shoot induction media (Table ).Effect of various cytokinins on in vitro shoot induction in Stevia rebaudiana


	Plant growth regulator (mg/l)	Number of shoots	Shoot length (cm)	Number of leaves	
BAP	Kinetin	TDZ	
A	0.0	0.0	0.0	1.50 ± 0.28a
	1.75 ± 0.14bcd
	14.50 ± 1.50bc
	
B	0.2	–	–	2.25 ± 0.25a
	1.20 ± 0.27abc
	15.75 ± 1.84bc
	
C	0.5	–	–	1.75 ± 0.25a
	0.80 ± 0.08ab
	15.25 ± 1.79bc
	
D	1.0	–	–	2.00 ± 0.00a
	0.45 ± 0.09a
	12.00 ± 1.63ab
	
E	–	0.2	–	2.00 ± 0.00a
	1.95 ± 0.35cde
	12.25 ± 0.25ab
	
F	–	0.5	–	1.50 ± 0.28a
	1.00 ± 0.08ab
	20.00 ± 0.81de
	
G	–	1.0	–	1.75 ± 0.25a
	1.15 ± 0.05abc
	17.50 ± 0.50cd
	
H	0.2	0.2	–	2.00 ± 0.00a
	3.10 ± 0.96f
	11.50 ± 0.50ab
	
I	0.5	0.5	–	2.25 ± 0.25a
	0.82 ± 0.19ab
	9.50 ± 0.95a
	
J	1.0	1.0	–	1.75 ± 0.25a
	0.95 ± 0.12ab
	8.00 ± 1.63a
	
K	–	–	0.01	2.00 ± 0.00a
	2.82 ± 0.11ef
	22.75 ± 0.94ef
	
L	–	–	0.03	2.00 ± 0.00a
	2.60 ± 0.21def
	23.50 ± 1.25ef
	
M	–	–	0.05	2.00 ± 0.00a
	2.77 ± 0.13ef
	24.25 ± 0.47f
	
N	–	–	0.1	2.00 ± 0.00a
	2.65 ± 0.12def
	24.75 ± 0.94fg
	
O	–	–	0.2	2.00 ± 0.00a
	2.30 ± 0.13def
	28.50 ± 1.25g
	
P	–	–	0.5	3.00 ± 0.57b
	2.20 ± 0.11def
	33.00 ± 2.88h
	
Parameters have been recorded after 3 weeks of culture. Data are in the form of mean ± SEM, and means followed by the same letter within the columns are not significantly different (P = 0.05) using Duncan’s multiple range test



Nodal explants inoculated on half strength MS media having concentration of TDZ (0.01, 0.03, 0.05, 0.1, 0.2, 0.5 mg/l) showed shoot development at every concentration (Fig. a); 0.5 mg/l was found to be the most effective, i.e., number of shoots (3.00 ± 0.57), shoot length (2.20 ± 0.11 cm) and number of leaves (33.00 ± 2.88) (Table ). It was noted that at lower concentration of TDZ, number of shoots was less and shoot length high, and on increasing the concentration, multiple shooting increases, but with decreased shoot length. Mithila et al. () reported that low concentration of TDZ induced shoot organogenesis of African violet explants, whereas at higher doses (5–10 μM) somatic embryos were formed. Among other agents with cytokinin activity, comparatively low amount of TDZ promotes shoot multiplication in several plants (Guo et al. ). It was also reported that TDZ induced better response than BA (6-benzyl adenine) in shoot regeneration in peanut (Victor et al. ; Gairi and Rashid, ). Role of TDZ in shoot induction and multiple shooting has been reported in other plants as well; the highest rate of shoot regeneration from Echinacea purpurea leaf explants cultured on medium with TDZ at 2.5 μM or higher was reported (Jones et al. ). TDZ, a phenyl-urea type plant growth regulator, was earlier used as a cotton defoliant (Arndt et al. ). Later, it was believed to exhibit strong cytokinin-like activity almost similar to that of N6-substituted adenine derivatives (Mok et al. ; Gyulai et al. ). In the present study, TDZ appears to mimic cytokinin-like activity causing the release of lateral buds (Wang et al. ) and showed better response in terms of shoot regeneration efficiency, compared to other cytokinins, similar to other findings where TDZ produced shoots comparable to or greater than that of other cytokinins (Fiola et al. ; Malik and Saxena ).In vitro shoot multiplication and rooting of Stevia rebaudiana.
a Shoot formation in ½ MS supplemented with 0.01 mg/l TDZ. b Cultures in induction media TDZ (0.5 mg/l) transferred to ½ MS multiplication media without hormone. c Cultures in induction media TDZ (0.5 mg/l) transferred to ½ MS multiplication media supplemented with TDZ (0.01 mg/l). d In vitro rooting in ¼ MS supplemented with IBA. e Hardened plants in perlite. f In vitro-raised Stevia rebaudiana in garden soil and sand mixture



At initial stage, best response of shoot induction, considering all the parameters together was observed at BAP (0.2 mg/l), Kn (0.2 mg/l), BAP/Kn (0.2 mg/l each) and TDZ (0.5 mg/l) (Table ). These best concentrations, respectively, denoted as B, E, H and P were selected and transferred to different shoot multiplication media (Table ).Effect of cytokinins on shoot multiplication of Stevia rebaudiana


Induction medium	Multiplication medium	Number of shoots	Shoot length (cm)	Number of leaves	
BAP (0.2 mg/l)	B	5.75 ± 0.25bc
	4.32 ± 0.48a
	46.25 ± 0.62cd
	
½ MS	3.25 ± 0.62a
	4.20 ± 0.16a
	37.50 ± 1.70b
	
½ MS + TDZ (0.01)	9.25 ± 0.25d
	6.67 ± 0.26d
	50.50 ± 0.95ef
	
Kn (0.2 mg/l)	E	4.75 ± 0.25b
	5.05 ± 0.09b
	44.75 ± 0.48c
	
½ MS	2.50 ± 0.28a
	4.82 ± 0.16ab
	30.50 ± 0.95a
	
½ MS + TDZ (0.01)	8.50 ± 0.28d
	6.95 ± 0.95d
	50.00 ± 0.81ef
	
BAP and Kn (0.2 mg/l each)	H	5.25 ± 0.25bc
	5.20 ± 0.14b
	40.75 ± 0.75b
	
½ MS	3.00 ± 0.40a
	5.90 ± 0.12c
	30.50 ± 1.70a
	
½ MS + TDZ (0.01)	8.75 ± 0.25d
	8.05 ± 0.22e
	49.00 ± 1.29de
	
TDZ (0.5 mg/l)	P	6.00 ± 0.40c
	4.85 ± 0.12ab
	52.50 ± 1.25f
	
½ MS	4.75 ± 0.25b
	5.02 ± 0.16b
	40.00 ± 0.81b
	
½ MS + TDZ (0.01)	11.00 ± 0.40e
	7.17 ± 0.16d
	61.00 ± 1.29g
	
Parameters have been recorded after 4 weeks of transfer in multiplication media. Data are in the form of mean ± SEM, and means followed by the same letter within the columns are not significantly different (P = 0.05) using Duncan’s multiple range test



Shoot multiplication
Shoots cultured in the same induction medium B, E, H, P and half MS medium devoid of growth regulator (Fig. b) produced minimal response of shoot multiplication; however, those sub-cultured in the half MS multiplication medium containing TDZ produced significantly higher number of shoots (Fig. c), the best response being observed in TDZ (0.01 mg/l). Shoots obtained from induction medium P containing TDZ (0.5 mg/l) produced highest number of shoots (11.00 ± 0.40) in half MS multiplication media containing 0.01 mg/l TDZ (Table ). However, shoot obtained from the induction medium B containing BAP (0.2 mg/l), E containing Kn (0.2 mg/l) and H containing BAP/Kn (0.2 mg/l each), produced (9.25 ± 0.25), (8.50 ± 0.28), (8.75 ± 0.25) shoots, respectively, when transferred separately in MS half containing 0.01 mg/l TDZ. The number of shoots obtained is lesser when compared with those obtained from medium P containing 0.5 mg/l TDZ (Table ).

Shoot elongation
Shoot length increased in all the media supplemented with TDZ (Fig. c). Shoot obtained from induction medium H (BAP/Kn 0.2 mg/l each) showed highest shoot length (8.05 ± 0.22) when transferred in half MS multiplication medium supplemented with 0.01 mg/l TDZ (Table ). It is to be noticed that there is a minute difference in shoot length of cultures obtained from induction medium H (BAP/Kn 0.2 mg/l each) and P (TDZ 0.5 mg/l), when they were transferred separately in half MS medium supplemented with 0.01 mg/l TDZ. The shoots sub-cultured in the same induction medium (B, E, H, P) and half MS medium devoid of growth regulator (Fig. b) did not significantly increase the length of the shoots (Table ). Also, it is observed that shoot cultured in the medium containing TDZ produced significantly more number of leaves (Fig. c). Highest number of leaves (61.00 ± 1.29) was observed in the multiplication media with ½ MS + TDZ (0.01 mg/l), the shoots when cultured in the same induction medium (B, E, H, P) and half MS medium devoid of growth hormone showed comparatively lesser number of leaves, i.e., in the range of 30–52 leaves (Table ).

Root induction and hardening
Elongated shoots were separated from the shoot multiplication media and transferred to the rooting media. Root induction was observed in all cultures supplemented with different concentrations of IBA (Fig. d). IBA added to one-fourth MS medium produced a better rooting response as compared to half MS (Table ). Highest number of roots (11.00) with longer roots (4.62 cm) was obtained on one-fourth MS medium supplemented with 1.0 mg/l IBA (Table ). Use of activated charcoal in the rooting media facilitated rooting, as also reported in other studies (Komalivalli and Rao ; Priyadarshini et al. ). Rooted plants were hardened in perlite (Fig. e) where all of them were healthy and viable for 3 weeks in the culture room. The survival rate of the transferred rooted plants into the pots containing garden soil and sand (1:1) was 80 %, after 2 months (Fig. f). Two-stage culture procedure seems to be a versatile protocol for efficient induction and multiple shoot formation, in Stevia similar to that noticed in Cassia angustifolia (Iram and Anis ) and Pterocarpus marsupium (Husain et al. ). A two-stage culture procedure has been developed for highly efficient shoot regeneration from leaf and internode explants of Bacopa monnieri (Ceasar et al. ).Effect of MS salt concentration and IBA on in vitro root induction

Rooting media	IBA (mg/l)	Number of roots	Root length (cm)	
½ MS media	0.2	1.75 ± 0.25a
	1.85 ± 0.09a
	
0.5	3.25 ± 0.25b
	1.87 ± 0.11a
	
1.0	6.00 ± 0.40c
	2.20 ± 0.18a
	
¼ MS media	0.2	2.75 ± 0.25ab
	2.12 ± 0.11a
	
0.5	6.25 ± 0.85c
	2.80 ± 0.18b
	
1.0	11.00 ± 0.40d
	4.62 ± 0.19c
	
Parameters have been recorded after 4 weeks of transfer in rooting media. Data are in the form of mean ± SEM, and means followed by the same letter within the columns are not significantly different (P = 0.05) using Duncan’s multiple range test



Stevioside content
The presence of the active principles was confirmed in both the in vivo- and in vitro-derived leaves of Stevia. The identification and quantification of stevioside content in the samples were done by comparing the retention time and peak area of sample with that of the standard. In samples of in vitro plants, HPLC analysis revealed that stevioside content was higher than those of in vivo plants. Initial study showed that stevioside production was tissue and age dependent (data not shown). The percentage stevioside content of the samples, in the in vivo and in vitro leaves of Stevia was found to be 7.017 ± 0.058 and 9.236 ± 0.046, respectively (Table ); the in vitro-raised plants had higher stevioside content.Stevioside content in in vivo and in vitro leaf samples

S. no.	Samples	Medium	Retention time (mm:ss)	Peak area (mAs)	Stevioside content (%)	
1	Standard	–	2:22	596.1	–	
2	In vivo plants	–	2:21	8,421.6	7.017 ± 0.058	
3	In vitro plants (3-week-old)	Half strength MS	2:24	11,040.7	9.236 ± 0.046	
4	In vitro-raised plants (8-week-old)	Half strength MS	2:24	11,040.7	9.236 ± 0.046	
Two replicates of each sample were used for HPLC analysis (mean value calculated)



In conclusion, the present study demonstrates an efficient micropropagation protocol of S. rebaudiana using two-stage culture procedures. Furthermore, the micropropagation protocol generated did not affect the content of stevioside in Stevia leaves as shown by its higher content in the in vitro-raised leaves. The protocol might be useful in germplasm conservation and stevioside production.

Abbreviations
MSMurashige and Skoog

BAPBenzylaminopurine

KnKinetin

TDZThidiazuron

IBAIndole-3-butyricacid

HPLCHigh performance liquid chromatography

The authors are thankful to the University Grants Commission (UGC), New Delhi for the financial assistance in the form of a major research project.

Conflict of interest
We declare that we do not have any conflict of interest in the publication. Further, we state that this manuscript is original, unpublished and not under simultaneous consideration by another journal.

