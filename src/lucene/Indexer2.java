/*
 * Indexer.java
 *
 * Created on 6 March 2006, 13:05
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package lucene;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.File;
import java.util.ArrayList;

import org.apache.lucene.document.BinaryDocValuesField;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field; 
import org.apache.lucene.document.StringField; 
import org.apache.lucene.document.TextField; 
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.Chunking;
import com.aliasi.dict.ExactDictionaryChunker;


public class Indexer2 {
    
    /** Creates a new instance of Indexer */
    public Indexer2() {
    }
 
    private IndexWriter indexWriter = null;
    
    public IndexWriter getIndexWriter(boolean create) throws IOException {
        if (indexWriter == null) {
            Directory indexDir = FSDirectory.open(new File("index-directory").toPath());
            IndexWriterConfig config = new IndexWriterConfig(new StandardAnalyzer());
            config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
            indexWriter = new IndexWriter(indexDir, config);
        }
        return indexWriter;
   }    
   
    public void closeIndexWriter() throws IOException {
        if (indexWriter != null) {
            indexWriter.close();
        }
   }  
    
    public void rebuildIndexes(ExactDictionaryChunker chunker) throws IOException {
          //
          // Erase existing index
          //
          getIndexWriter(true);
          //
          // Index all Accommodation entries
          //
          
          File dir = new File("./pubmed/txt/3_Biotech");
          File[] files = dir.listFiles();
          for (File file : files)
          {
        	  indexWriter = getIndexWriter(false);
        	  Document document = new Document();

        	  String path = file.getCanonicalPath();
        	  document.add(new StringField("path", path, Field.Store.YES));

        	  Reader reader = new FileReader(file);
//        	  document.add(new TextField("contents", reader));
        	  document.add(new StringField("filename", file.getName(), Field.Store.YES));
        	  ArrayList<String> listIndex = findIndex(new FileReader(file), chunker);
        	  
        	  
        	  StringBuilder  stringBuilder = new StringBuilder();
      	   	  String         ls = System.getProperty("line.separator");
      	   	  BufferedReader r = new BufferedReader(reader);
      	   	  String         line = null;

      	    try {
      			while( ( line = r.readLine() ) != null ) {
      			    stringBuilder.append( line );
      			    stringBuilder.append( ls );    
      			
      			}
      			
      		  reader.close();
      		} catch (IOException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		}
        	  
      //	    document.add(new TextField("content", stringBuilder.toString(), Field.Store.NO));
        	String ont = "";
  	  
        	  for (String s: listIndex)
        	  {
        		  ont += s + System.getProperty("line.separator");
        		  document.add(new StringField("ont", s, Field.Store.YES));
            	  
        	  }
//        	  System.out.println(ont);
//        	  document.add(new StringField("ont", listIndex.get(0), Field.Store.YES));
        	  indexWriter.addDocument(document);
        	 
          }
//          indexWriter.addDocument(document);
          //
          // Don't forget to close the index writer when done
          //
          closeIndexWriter();
     }  
    
    public ArrayList<String> findIndex(FileReader fileReader, ExactDictionaryChunker chunker)
    {
    	
    	ArrayList<String> listIndex = new ArrayList<String>();
    	
	 	BufferedReader reader = new BufferedReader(fileReader);
	    String         line = null;
	    StringBuilder  stringBuilder = new StringBuilder();
	    String         ls = System.getProperty("line.separator");
	 
	    
	    try {
			while( ( line = reader.readLine() ) != null ) {
			    stringBuilder.append( line );
			    stringBuilder.append( ls );
			}
			
			  reader.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
    	Chunking chunking = chunker.chunk(stringBuilder.toString());
    	
    	
    	for (Chunk chunk: chunking.chunkSet())
    	{
    		
    		int start = chunk.start();
	        int end = chunk.end();
	        String type = chunk.type();
	        double score = chunk.score();
	        String phrase = stringBuilder.toString().substring(start,end);
	        listIndex.add(phrase);
	               
    	}
    
    	return listIndex;
    }
}
