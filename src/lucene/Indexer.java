package lucene;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ont.MyOntClass;
import ont.SemType;
import ont.SerializeObject;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.Chunking;
import com.aliasi.dict.ExactDictionaryChunker;

public class Indexer {
    
    /** Creates a new instance of Indexer */
    public Indexer() {
    }
 
    private IndexWriter indexWriter = null;
    
    public IndexWriter getIndexWriter(boolean create, String file) throws IOException 
    {
        if (indexWriter == null) {
            Directory indexDir = FSDirectory.open(new File(file).toPath());
            IndexWriterConfig config = new IndexWriterConfig(new StandardAnalyzer());
            config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
            indexWriter = new IndexWriter(indexDir, config);
        }
        return indexWriter;
   }    
   
    public void closeIndexWriter() throws IOException {
        if (indexWriter != null) {
            indexWriter.close();
        }
   }  
    
    public void rebuildIndexesPubMed() throws IOException
    {
    	//
    	// Erase existing index
    	//
    	getIndexWriter(true, "index-pubmed");
    	//
    	// Index all Accommodation entries
    	//
    	
		File dir = new File("./pubmed/txt/");
		File[] dirs = dir.listFiles();
		
		System.out.println(dirs.length);
		
		for (File d: dirs)
		{
			File[] files = d.listFiles();
			
			
			for (File file : files)
			{
				indexWriter = getIndexWriter(false, "index-pubmed");
				Document document = new Document();
	
				String path = file.getCanonicalPath();
				
		//		System.out.println(path);
				document.add(new StringField("path", path, Field.Store.YES));
				
				FileReader reader = new FileReader(file);
				//         	  document.add(new TextField("contents", reader));
				document.add(new StringField("filename", file.getName(), Field.Store.YES));      	  
				
				StringBuilder  stringBuilder = new StringBuilder();
				String         ls = System.getProperty("line.separator");
				BufferedReader r = new BufferedReader(reader);
				String         line = null;
			
				try {
					while((line = r.readLine()) != null) 
					{
					    stringBuilder.append( line );
					    stringBuilder.append( ls );	
					}
			
				  reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
				e.printStackTrace();
				}
				document.add(new TextField("content", stringBuilder.toString(), Field.Store.YES));
				
				indexWriter.addDocument(document);
			}
		}
		
		closeIndexWriter();
    }
    
    
    public void rebuildIndexesOnt() throws IOException
    {
    	//
    	// Erase existing index
    	//
    	getIndexWriter(true, "index-ont");
    	//
    	// Index all Accommodation entries
    	//
     
    	SerializeObject so = new SerializeObject();	     
    	HashMap<String, ArrayList<ont.MyOntClass>> merge = new HashMap<String, ArrayList<ont.MyOntClass>>();    
    	merge = so.DeserializeMergeHashMap("merge");
    	
    	indexWriter = getIndexWriter(false, "index-ont");

    	for (Iterator<Map.Entry<String, ArrayList<MyOntClass>>> k = merge.entrySet().iterator(); k.hasNext();)
    	{
    		Map.Entry<String, ArrayList<MyOntClass>> e = k.next();
    		
    		Document document = new Document();
    		
    		String label = e.getKey();
    		
    		ArrayList<String> nested = new ArrayList<String>();
    		
    		for (MyOntClass cls: e.getValue())
    		{
    			String realLabel = cls.getRealLabel();
    			String definition = cls.getDefinition();
    			String file = cls.getFileName();
    			String id = cls.getId();
    			String ont = cls.getId().replaceAll("[^a-zA-Z]", "");
    			SemType sem = cls.getSemType();
    			
    			for (String terms: cls.getNestedList())
    			{
    				if (!nested.contains(terms))
    				{
    					nested.add(terms);
    					document.add(new StringField("nested", terms, Field.Store.NO));
    				}
    			}
    			document.add(new TextField("realLabel", realLabel, Field.Store.NO));
    		
    			if (definition != null)
    				document.add(new TextField("definition", definition, Field.Store.NO));
    			document.add(new StringField("file", file, Field.Store.NO));
    			document.add(new StringField("id", id, Field.Store.NO));
    			document.add(new StringField("ont", ont, Field.Store.NO));
    			document.add(new TextField("fullsem", sem.getLabel(), Field.Store.YES));
    			document.add(new StringField("sem", sem.getAcronym(), Field.Store.YES));		
  			
    		}
    		
    		FieldType BodyOptions = new FieldType(); 
    //		BodyOptions.setIndexed(true);
    		BodyOptions.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
    		BodyOptions.setStored(true); 
    		BodyOptions.setStoreTermVectors(true); 
    		BodyOptions.setTokenized(true); 
    		document.add(new Field("label2", label, BodyOptions));
    		
    		document.add(new TextField("label", label, Field.Store.YES));
    		
    		indexWriter.addDocument(document);
    	}
    	//
    	// Don't forget to close the index writer when done
    	//
    	closeIndexWriter();
     }  
}
