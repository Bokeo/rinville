/*
 * Main.java
 *
 * Created on 6 March 2006, 11:51
 *
 */

package lucene;

import ont.MyOntClass;
import ont.NormHM;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ont.SerializeObject;

import org.apache.log4j.Logger;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;

import org.apache.lucene.document.Document;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.IndexSearcher;

import com.aliasi.chunk.Chunking;
import com.aliasi.dict.DictionaryEntry;
import com.aliasi.dict.ExactDictionaryChunker;
import com.aliasi.dict.MapDictionary;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;


public class Main {
	
	static Logger log = Logger.getLogger(Main.class);
    
    
	public static void countFiles()
	{
		File dir = new File("./pubmed/txt/");
		File[] dirs = dir.listFiles();
		
		System.out.println(dirs.length);
		int c = 0;
		
		for (File d: dirs)
		{
			File[] files = d.listFiles();
			
			c += files.length;
		}
			
		System.out.println(c);
	}
	
	
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    	
    	lucene luc = new lucene("index-pubmed", "content");
    	luc.buildIndexPubMed();
        
        try 
   	    {
        	NormHM nh = new NormHM();
        	nh.openOutputFile("query");
        	  	
        	nh.writeOutput(luc.query("synapse"));
	    
			nh.CloseOutputFile();
			
	//		countFiles();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	    
    }
	
}
