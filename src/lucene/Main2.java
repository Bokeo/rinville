package lucene;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ont.NormHM;
import ont.SemType;

import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

public class Main2 {
	
	static Logger log = Logger.getLogger(Main2.class);
    
    public static void main(String[] args) {
    	    
    	
    	lucene luc = new lucene("index-ont", "label");
    	luc.buildIndexOnt();
        
        try 
   	    {
        	NormHM nh = new NormHM();
        	nh.openOutputFile("query");
        	
        	nh.writeOutput(luc.query("synapse", "mp"));
	    
			nh.CloseOutputFile();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
        
        luc.searchFreqTerm("polymerase");
        luc.getIdsDoc();
    }
}
