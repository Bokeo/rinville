package lucene;

import java.io.IOException;
import java.util.HashMap;

import ont.NormHM;

import org.apache.log4j.Logger;

public class Main3 {
	
	static Logger log = Logger.getLogger(Main3.class);
    
    public static void main(String[] args) {
    	    
    	
    	lucene luc = new lucene("index-ont", "label");
    	luc.buildIndexOnt();
        
        HashMap<String, HashMap<String, Integer>> freqs = new HashMap<String, HashMap<String, Integer>>();
        
        freqs = luc.getFreqs();
        
        
        lucene luc2 = new lucene("index-pubmed", "content");
   
        // you can comment the line below if you already have indexed the file.
    	luc.buildIndexPubMed();

        try 
   	    {
        	NormHM nh = new NormHM();
     
        	nh.openOutputFile("query");
        	nh.writeOutput(luc2.query("synaptic transmission", freqs));    
			nh.CloseOutputFile();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
    }
}
