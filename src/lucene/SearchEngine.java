/*
 * SearchEngine.java
 *
 * Created on 6 March 2006, 14:52
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package lucene;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ont.SemType;

import org.apache.lucene.document.Document;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.TotalHitCountCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;



public class SearchEngine {
    private IndexSearcher searcher = null;
    private QueryParser parser = null;
       
    /** Creates a new instance of SearchEngine */
    public SearchEngine(String dir, String def) {
        try {
        	
			searcher = new IndexSearcher(DirectoryReader.open(FSDirectory.open(new File(dir).toPath())));
        
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        parser = new QueryParser(def, new StandardAnalyzer());
    }
    
    public TopDocs performSearch(String queryString, int n)
    throws IOException, ParseException {
    	
    	String newQuery = QueryParser.escape(queryString);
        Query query = parser.parse(newQuery);  
        
        return searcher.search(query, n);
       
    }
    
    public TopDocs performSearch(String queryString, int n, HashMap<String, HashMap<String, Integer>> freqs)
    	    throws IOException, ParseException {	
    	
    	    	String newQuery = QueryParser.escape(queryString);
    	    	
    	    	String[] newQ = newQuery.toLowerCase().split(" ");
    	    	
    	    	String newQuery2 = "";
    	    	int i = 0;
    	    	
    	    	newQuery2 += "(" + newQuery + ") AND ";
    	    	
    	    	for (String s: newQ)
    	    	{
    	    		int value = 1;
    	    		
    	    		for (Iterator<Map.Entry<String, HashMap<String, Integer>>> k = freqs.entrySet().iterator(); k.hasNext();)
    	    		{
    	    			Map.Entry<String, HashMap<String, Integer>> e = k.next();
    	    			
    	    			if (e.getValue().containsKey(s))
    	    			{
//    	    				value += e.getValue().get(s);
    	    				++value;
    	    			}	
    	    		}
    	    		
    	    		if (i == 0)
    	    			newQuery2 += "(content:" + s + "^" + value;
    	    		else 
    	    			newQuery2 += " OR content:" + s + "^" + value;
	    		
    	    		++i;
    	    	}
    	    	
    	    	
    	    	newQuery2 += ")";
    	    	System.out.println(newQuery2);
    	    	
    	        Query query = parser.parse(newQuery2);  
    	        
    	        return searcher.search(query, n);
    	       
    	    }
    
    public TopDocs performSearch(String queryString, String queryString2, int n, HashMap<String, SemType> allSems)
    	    throws IOException, ParseException {
    	    	
    	    	String newQuery = QueryParser.escape(queryString);
    	        String newQuery2 = QueryParser.escape(queryString2); 
    	        
    	        ArrayList<SemType> semTypeList = new ArrayList<SemType>();
    	        String fullQuery = "";
    	        
    	        String[] newQ = newQuery.toLowerCase().split(" ");
    	        
    	        
    	        if (allSems.containsKey(newQuery2))
    	        {
    	        	semTypeList.add(allSems.get(newQuery2));
    	        	getSemType(allSems.get(newQuery2), semTypeList);
 		        	
    	        	fullQuery += "(" + newQuery + ")";
    	        	
    	        	fullQuery += " AND +(" + "fullsem:\"" + semTypeList.get(0).getLabel() 
    	        			+ "\" OR sem:" + semTypeList.get(0).getAcronym() + "";
  	        			   
    	        	for (int i = 1; i < semTypeList.size(); ++i)
    	        	{	        		
    	        		fullQuery += " OR fullsem:\"" + semTypeList.get(i).getLabel() 
    	        				+ "\" OR sem:" + semTypeList.get(i).getAcronym() + "";
    	        	}
    	        	
    	        	fullQuery += ")";
    	        
    	        }
    	        else
    	        	fullQuery = newQuery + " AND (sem:\"all\")";
 
    	        return searcher.search(parser.parse(fullQuery), n);    	       
    	    }

    public Document getDocument(int docId)
    throws IOException {
        return searcher.doc(docId);
    }
    
    public void getSemType(SemType sem, ArrayList<SemType> semTypeList)
    {
    	for (SemType s: sem.getChildren())
    	{
    		semTypeList.add(s);
    		getSemType(s, semTypeList);
    	}
    }
}
