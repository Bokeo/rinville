package lucene;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ont.MapUtil;
import ont.NormHM;
import ont.NormLingPipe;
import ont.SemType;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Fields;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

public class lucene {
	
	static Logger log = Logger.getLogger(lucene.class);
	
	private Indexer indexer = null;
	private SearchEngine se = null;
	
	public lucene(String dir, String def)
	{
		indexer = new Indexer();
		se = new SearchEngine(dir, def);
	}
	
	
	public void buildIndexOnt()
	{
		try 
		{
			log.info("begin rebuild index");
			indexer.rebuildIndexesOnt();
			log.info("index done");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void buildIndexPubMed()
	{
		try 
		{
			log.info("begin rebuild index");
			indexer.rebuildIndexesPubMed();
			log.info("index done");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String query(String queryString)
	{
		StringBuilder sb = new StringBuilder();
		
		try
		{
		//	log.info("performSearch");
		    	    
		    NormHM nh = new NormHM();
		    HashMap<String, SemType> allSems = new HashMap<String, SemType>();
		    
		    nh.createSemType(allSems);	
		   
			TopDocs topDocs;
			
			topDocs = se.performSearch(queryString, 300000);
		
			if (topDocs.totalHits > 0)
				sb.append("Results found: " + topDocs.totalHits + System.getProperty("line.separator"));
			
			ScoreDoc[] hits = topDocs.scoreDocs;
			    	        
		    for (int i = 0; i < hits.length; i++)
		    {
		        Document doc = se.getDocument(hits[i].doc);
		      		       
		        sb.append("result " + i + ": " + doc.get("label"));
		        sb.append(System.getProperty("line.separator"));
		        
	        }
		    
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	
	public String query(String queryString, String semantic)
	{
		StringBuilder sb = new StringBuilder();
		
		try
		{
		//	log.info("performSearch");
		    	    
		    NormHM nh = new NormHM();
		    HashMap<String, SemType> allSems = new HashMap<String, SemType>();
		    
		    nh.createSemType(allSems);	
		   
			TopDocs topDocs;
			
			topDocs = se.performSearch(queryString, semantic, 10000, allSems);
		
			if (topDocs.totalHits > 0)
				sb.append("Results found: " + topDocs.totalHits + System.getProperty("line.separator"));
			
			ScoreDoc[] hits = topDocs.scoreDocs;
			    	        
		    for (int i = 0; i < hits.length; i++)
		    {
		        Document doc = se.getDocument(hits[i].doc);
		        
		        ArrayList<SemType> semTypeList = new ArrayList<SemType>();
		       
		        if (allSems.containsKey(semantic))
		        {
		        	semTypeList.add(allSems.get(semantic));
		        	se.getSemType(allSems.get(semantic), semTypeList);
		        }

		        for (int j = 0; j < doc.getValues("sem").length; ++j)
		        {
		        	if (semTypeList.contains(allSems.get(doc.getValues("sem")[j]))
		        			|| semTypeList.contains(allSems.get(doc.getValues("fullsem")[j])))
		        	{
		        		sb.append("result " + i + ": " + doc.getValues("label")[0] 
				        		+ " [" + doc.getValues("fullsem")[j]
				        		+ " (" + doc.getValues("sem")[j] + ")]"
				        		+ System.getProperty("line.separator"));
		        	}  	
		        }
	        }
		    
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	public String query(String queryString, HashMap<String, HashMap<String, Integer>> freqs)
	{
	StringBuilder sb = new StringBuilder();
		
		try
		{
		//	log.info("performSearch");
		    	    
		    NormHM nh = new NormHM();
		    HashMap<String, SemType> allSems = new HashMap<String, SemType>();
		    
		    nh.createSemType(allSems);	
		   
			TopDocs topDocs;
				
			topDocs = se.performSearch(queryString, 300000, freqs);
			
			if (topDocs.totalHits > 0)
				sb.append("Results found: " + topDocs.totalHits + System.getProperty("line.separator"));
			
			ScoreDoc[] hits = topDocs.scoreDocs;
			    	        
		    for (int i = 0; i < hits.length; i++)
		    {
		        Document doc = se.getDocument(hits[i].doc);
		      		       
		        sb.append("result " + i + ": " + doc.get("filename"));
		        sb.append(System.getProperty("line.separator"));
		        
	        }
		    
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	public HashMap<String, HashMap<String, Integer>> getFreqs()
	{
		HashMap<String, HashMap<String, Integer>> freqs = new HashMap<String, HashMap<String, Integer>>();
		
		try {
			
			NormHM nh = new NormHM();
			ArrayList<ArrayList<String>> listSem = new ArrayList<ArrayList<String>>();
			
		//	listSem = nh.listAllSemType();
			listSem = nh.listAllSemChildren();
			
			
			IndexSearcher searcher = new IndexSearcher(DirectoryReader.open(FSDirectory.open(new File("index-ont").toPath())));
			IndexReader reader = DirectoryReader.open(FSDirectory.open(new File("index-ont").toPath()));
			
			for (ArrayList<String> list: listSem)
			{
				HashMap<String, Integer> freqTerms = new HashMap<String, Integer>();	
				
				String queryString = "";
				
				int j = 0;
				
				for (String s: list)
				{
					if (j == 0)
						queryString += s;
					else
						queryString += " OR " + s;
					
					++j;
				}
				
				TopDocs topdocs = searcher.search(new QueryParser("sem", new StandardAnalyzer()).parse(queryString), 305000);
				
				ScoreDoc[] hits = topdocs.scoreDocs;        
				ArrayList<Integer> setDoc = new ArrayList<Integer>();
				
				if (topdocs.totalHits == 0)
					continue;
				
			    for (int i = 0; i < hits.length; i++)
			    {
			    	int docId = hits[i].doc;
			    	
			    	setDoc.add(docId);
			
			    	Terms terms = reader.getTermVector(docId, "label2");
			    	TermsEnum iterator = terms.iterator();
			    	    	
			        BytesRef byteRef = null;
			        while((byteRef = iterator.next()) != null)
			        {	    	   
			        	String term = byteRef.utf8ToString();
			    	  
			        	if (freqTerms.containsKey(term))
			        		freqTerms.put(term, freqTerms.get(term) + 1);
			        	else
			        		freqTerms.put(term, 1);	
			       }
			    }
			  
			    System.out.println(list.get(0) + ": " + freqTerms.size());
			    
			    freqTerms = (HashMap<String, Integer>) MapUtil.sortByValue(freqTerms);
			    freqs.put(list.get(0), freqTerms);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		
		return freqs;
	}
	
	public void getIdsDoc()
	{
		try {
		
			HashMap<String, HashMap<String, Integer>> freqs = new HashMap<String, HashMap<String, Integer>>();
			
			freqs = getFreqs();
			
			compareFreqs(freqs);
			statTerms(freqs);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
 	}
	
	public void compareFreqs(HashMap<String, HashMap<String, Integer>> freqs)
	{
		
		NormHM nh = new NormHM();
		
		try {
		

			
		for (Iterator<Map.Entry<String, HashMap<String, Integer>>> k = freqs.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, HashMap<String, Integer>> e = k.next();
			
			HashMap<String, Integer> freqTerms = e.getValue();			
			
			for (Iterator<Map.Entry<String, HashMap<String, Integer>>> z = freqs.entrySet().iterator(); z.hasNext();)
			{
				Map.Entry<String, HashMap<String, Integer>> y = z.next();
					
				if (e.getKey().equals(y.getKey()))
					continue;	
									
				nh.openOutputFile("freqs/" + e.getKey()+ "_" + y.getKey());
				
				HashMap<String, Integer> freqTerms2 = y.getValue();				
				
				nh.writeOutput(compareTerms(freqTerms, freqTerms2));
				nh.CloseOutputFile();
			}
		}
		
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public String compareTerms(HashMap<String, Integer> freqTerm, HashMap<String, Integer> freqTerm2)
	{
		String output = "";
		
		for (Iterator<Map.Entry<String, Integer>> k = freqTerm.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, Integer> e = k.next();
			
			if (freqTerm2.containsKey(e.getKey()))
			{
				output += e.getKey() + " " + e.getValue() + " " + freqTerm2.get(e.getKey());
				output += System.getProperty("line.separator");
			}
			else
			{
				output += e.getKey() + " " + e.getValue() + " 0";
				output += System.getProperty("line.separator");
			}
		}
		
		for (Iterator<Map.Entry<String, Integer>> k = freqTerm2.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, Integer> e = k.next();
			
			if (!freqTerm.containsKey(e.getKey()))
			{
				output += e.getKey() + " 0 " + e.getValue();
				output += System.getProperty("line.separator");
			}
		}
		
		return output;
	}
	
	public void getAllTerms()
	{
		
		try {
			
			IndexReader reader = DirectoryReader.open(FSDirectory.open(new File("index-ont").toPath()));
		
			Fields fields = MultiFields.getFields(reader);
	        Terms terms = fields.terms("field");
	        TermsEnum iterator = terms.iterator();
	        
	        BytesRef byteRef = null;
	        while((byteRef = iterator.next()) != null) {
	            String term = new String(byteRef.bytes, byteRef.offset, byteRef.length);
	
	        }
        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void searchFreqTerm(String term)
	{
		try {
			IndexReader reader = DirectoryReader.open(FSDirectory.open(new File("index-ont").toPath()));
	
		 	String termText = term.toString();
	        Term termInstance = new Term("label", term);                            
	        long termFreq = reader.totalTermFreq(termInstance);
	        long docCount = reader.docFreq(termInstance);
	        	        
	        System.out.println("term: "+termText+", termFreq = "+termFreq+", docCount = "+docCount);
	        
	       
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void statTerms(HashMap<String, HashMap<String, Integer>> freqs)
	{
		
		NormHM nh = new NormHM();
		
		try {
			nh.openOutputFile("StatFreqs");
		
		for (Iterator<Map.Entry<String, HashMap<String, Integer>>> k = freqs.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, HashMap<String, Integer>> e = k.next();
			
			String output = "";
			int i = 0;
			
			output += e.getKey() + ": " + e.getValue().size() + " terms";
			output += System.getProperty("line.separator");
			
			for (Iterator<Map.Entry<String, Integer>> z = e.getValue().entrySet().iterator(); z.hasNext();)
			{
				Map.Entry<String, Integer> f = z.next();
				
				if (i < 10)
				{
					output += "\t" + f.getKey() + ": " + f.getValue();
					output += System.getProperty("line.separator");
				}	
				++i;
			}
			
			nh.writeOutput(output);
		}
		
		for (Iterator<Map.Entry<String, HashMap<String, Integer>>> k = freqs.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, HashMap<String, Integer>> e = k.next();
			
			HashMap<String, Integer> freqTerms = e.getValue();
			
			for (Iterator<Map.Entry<String, HashMap<String, Integer>>> z = freqs.entrySet().iterator(); z.hasNext();)
			{
				Map.Entry<String, HashMap<String, Integer>> y = z.next();
				
				if (e.getKey().equals(y.getKey()))
					continue;
				
				String output = "The 10 most frequent common terms between ";
				output += e.getKey() + " and " + y.getKey();
				output += System.getProperty("line.separator");
			
				HashMap<String, Integer> freqTerms2 = y.getValue();	
				int i = 0;
				
				for (Iterator<Map.Entry<String, Integer>> l = freqTerms.entrySet().iterator(); l.hasNext();)
				{
					Map.Entry<String, Integer> f = l.next();
									
					if (freqTerms2.containsKey(f.getKey()))
					{
						if (i < 10)
						{
							output += "\t" + f.getKey() + " " + f.getValue() + " " + freqTerms2.get(f.getKey());
							output += System.getProperty("line.separator");
						}
						++i;
					}
				}
				nh.writeOutput(output);
			}
		}
	
		nh.CloseOutputFile();
		
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

}
