package ont;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import monq.jfa.CompileDfaException;
import monq.jfa.Dfa;
import monq.jfa.DfaRun;
import monq.jfa.FaAction;
import monq.jfa.Nfa;
import monq.jfa.ReSyntaxException;
import monq.jfa.ReaderCharSource;
import monq.jfa.actions.Printf;


public class testNfa {
	
	static Logger log = Logger.getLogger(testNfa.class);
	
	public static void main(String[] args) {
		
		log.info("begin");
		
		SerializeObject so = new SerializeObject();
		
		HashMap<String, ArrayList<MyOntClass>> classes = so.DeserializeMergeHashMap("merge");
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\n");
		for (Iterator<Map.Entry<String, ArrayList<MyOntClass>>> k = classes.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, ArrayList<MyOntClass>> e = k.next();
				
			sb.append(e.getKey() + "\n"); 
		
		}
		
		
		
	    
	    PrintStream testfile = null;
		try {
			testfile = new PrintStream(new File ("./testnafa.txt"));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	    int z = 0;
	    
	    ArrayList<String> actionList = new ArrayList<String>();
		ArrayList<String> patternList = new ArrayList<String>();
		
		int j = 0;
	    
		for (Iterator<Map.Entry<String, ArrayList<MyOntClass>>> k = classes.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, ArrayList<MyOntClass>> e = k.next();
					
//			actionList.add("(" + e.getKey() + ")");
//			testPattern += "(([\r\n])(!(([\r\n])^)*(" + e.getKey() + ")(([\r\n])^)*)([\r\n]))";
			
			patternList.add("(" + e.getKey() + ")");
		//	actionList.add("<%(0,0,><)(0,0)>");
			actionList.add("<%0>");
//			testPattern += "(([\r\n])(!(([\r\n])^)*(" + e.getKey() + ")(([\r\n])^)*)([\r\n]))";
			
			++j;
			
			if (j == 50000)
				break;
			
		}
		
		
			try {	
				
				Nfa nfa = new Nfa(Nfa.NOTHING);
				 // Now we add all pattern/action pairs found on the command line. 
			    for(int i=0; i < patternList.size(); ++i) {
			      String pattern = patternList.get(i);
			      FaAction action = new Printf(true, actionList.get(i));
				
			      nfa.or(pattern, action);
			    }
			 
			    
			    log.info("before nfa compile");
			    
//				FaAction action = new Printf(true, actionList.get(0));
//			    nfa.or(testPattern, action);
		
			    // Compile the Nfa into a Dfa while specifying how input should be
			    // treated that does not match any of the regular expressions.
			    Dfa dfa = nfa.compile(DfaRun.UNMATCHED_COPY);

			    log.info("before dfa run");
			    // get a machinery (DfaRun) to operate the Dfa
			    DfaRun r1 = new DfaRun(dfa);

			    
			    
			    
			    log.info("begin part 1");
			    
			    InputStream inputStream = new ByteArrayInputStream(sb.substring(0, sb.length()).toString().getBytes());
			    // Specify the input and filter it to the output
			    r1.setIn(new ReaderCharSource(inputStream));
			    r1.filter(testfile);
			    
			    testfile.close();
			    inputStream.close();
			    
			   
			    
				} catch (ReSyntaxException | CompileDfaException | IOException exc) {
					// TODO Auto-generated catch block
					exc.printStackTrace();
				
				}
				log.info("end");
			
			
	//		System.out.println(testPattern);
			
		//	break;
		}
		
//		ArrayList<String> actionList = new ArrayList<String>();
//		ArrayList<String> patternList = new ArrayList<String>();
//		
//		
////		patternList.add("(oxide)");
////		actionList.add("<test>%0<test>");
////		patternList.add("([\r\n])(!(([\r\n])^)*(nitric oxide)(([\r\n])^)*)([\r\n])");
//		patternList.add("(nitric oxide)");
//		actionList.add("<%(0,0,><)(0,0)>");
//		patternList.add("(oxide biosynthetic)");
////		patternList.add("([\r\n])(!(([\r\n])^)*(oxide biosynthetic)(([\r\n])^)*)([\r\n])");
//		actionList.add("<%(0,0,><)(0,0)>");
//		
//		String test = "nitric oxide biosynthetic process";
//		InputStream inputStream = new ByteArrayInputStream(test.getBytes());
//		
//		try {	
//			
//			Nfa nfa = new Nfa(Nfa.NOTHING);
//			 // Now we add all pattern/action pairs found on the command line. 
//		    for(int i=0; i < patternList.size(); ++i) {
//		      String pattern = patternList.get(i);
//		      FaAction action = new Printf(true, actionList.get(i));
//			
//		      nfa.or(pattern, action);
//		     
//		      
//		    }
//		    
//		    
//		    
//		    // Compile the Nfa into a Dfa while specifying how input should be
//		    // treated that<%(0,0,><)(0,0)> does not match any of the regular expressions.
//		    Dfa dfa = nfa.compile(DfaRun.UNMATCHED_COPY);
//
// 
//		    // get a machinery (DfaRun) to operate the Dfa
//		    DfaRun r1 = new DfaRun(dfa);
//
//		    // Specify the input and filter it to the output
//		    r1.setIn(new ReaderCharSource(inputStream));
//		    r1.filter(System.out);
//		    
//		    
//			} catch (ReSyntaxException | CompileDfaException | IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		log.info("End");
//	}
	
	

}
