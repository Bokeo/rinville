package ont;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.*;
import org.apache.jena.util.FileManager;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.VCARD;

import java.io.*;
import java.util.Iterator;

/** Tutorial 5 - read RDF XML from a file and write it to standard out
 */
public class test extends Object {

    /**
        NOTE that the file is loaded from the class-path and so requires that
        the data-directory, as well as the directory containing the compiled
        class, must be added to the class-path when running this and
        subsequent examples.
    */    
    static final String inputFileName  = "./hpo.xrdf";
                              
    public static void main (String args[]) {
        // create an empty model
       // Model model = ModelFactory.createDefaultModel();

    	OntModel model = ModelFactory.createOntologyModel();
    	
        InputStream in = FileManager.get().open( inputFileName );
        if (in == null) {
            throw new IllegalArgumentException( "File: " + inputFileName + " not found");
        }
        
        // read the RDF/XML file
        model.read(in, "");
                      
        ExtendedIterator classes = model.listClasses();
      
        // list all classes
        
        while (classes.hasNext()) {
	        
        	OntClass cls = (OntClass) classes.next();
        	
        	System.out.println("New Class");
 
        // list all properties of the classes
        	ExtendedIterator<OntProperty> propIter = cls.listDeclaredProperties(); 
        	while (propIter.hasNext()) {
        		OntProperty property = (OntProperty) propIter.next(); 
        		System.out.println(property.getLocalName() + " " + cls.getPropertyValue(property));
        	} 
        	
        // the id and the label
	        System.out.println("Classes: " + cls.getLocalName() + "   " 
	        + cls.getRequiredProperty(RDFS.label).getString());
	        
	     
	        
	        
	       
//			SubsClass
//	        
//	        for (Iterator i = cls.listSubClasses(true); i.hasNext();) {
//		        OntClass c = (OntClass) i.next();
//		        System.out.print(" " + c.getLocalName() + "\n");
//	       
//	        
//	        } // end for
        }
        // write it to standard out
       // model.write(System.out);   
        
        model.close();
    }
}