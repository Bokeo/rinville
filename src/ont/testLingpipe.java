package ont;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.Chunking;
import com.aliasi.dict.DictionaryEntry;
import com.aliasi.dict.ExactDictionaryChunker;
import com.aliasi.dict.MapDictionary;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;


public class testLingpipe {
	
static Logger log = Logger.getLogger(testLingpipe.class);

static final double CHUNK_SCORE = 1.0;
	
	public static void main(String[] args) {
		
		SerializeObject so = new SerializeObject();
		NormHM nh = new NormHM();
		
		HashMap<String, ArrayList<MyOntClass>> classes = so.DeserializeMergeHashMap("merge");
		HashMap<String, ArrayList<MyOntClass>> merge = new HashMap<String, ArrayList<MyOntClass>>();
		
		MapDictionary<String> dictionary = new MapDictionary<String>();
		MapDictionary<String> dictionaryWithSyn = new MapDictionary<String>();
		
		for (Iterator<Map.Entry<String, ArrayList<MyOntClass>>> k = classes.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, ArrayList<MyOntClass>> e = k.next();	
			
			for (MyOntClass cls: e.getValue())
			{
				for (String s: cls.getSynList())
				{

					DictionaryEntry<String> de = new DictionaryEntry<String>(s, e.getKey(), CHUNK_SCORE);
					dictionaryWithSyn.addEntry(de);
				}
			}
			
			DictionaryEntry<String> de = new DictionaryEntry<String>(e.getKey(), e.getKey(), CHUNK_SCORE);
			dictionary.addEntry(de);
			dictionaryWithSyn.addEntry(de);
		
		}
		
		ExactDictionaryChunker dictionaryChunkerTF
	            = new ExactDictionaryChunker(dictionary,
	                                         IndoEuropeanTokenizerFactory.INSTANCE,
	                                         true,false);
		
		ExactDictionaryChunker dictionaryWithSynChunkerTF
        = new ExactDictionaryChunker(dictionaryWithSyn,
                                     IndoEuropeanTokenizerFactory.INSTANCE,
                                     true,false);


	    //    System.out.println("\nDICTIONARY\n" + dictionary);

		try {
    		
			String output = "";
		
			nh.openOutputFile("testLingpipe");
	        
		    for (Iterator<Map.Entry<String, ArrayList<MyOntClass>>> k = classes.entrySet().iterator(); k.hasNext();)
		    {
		    	Map.Entry<String, ArrayList<MyOntClass>> e = k.next();
		    	
		    	String text = "";
		    	
		    	for (MyOntClass cls: e.getValue())
		    	{
		    		text += System.getProperty("line.separator");
		    		
		    		for (String s: cls.getSynList())
		    			text += s + System.getProperty("line.separator");
		    	}
		    	
		    	text += e.getKey();
		    	
		    	ArrayList<String> listNested = chunk(dictionaryChunkerTF,text, nh, e.getKey());
		    	ArrayList<String> listNestedWithSyn = chunk(dictionaryWithSynChunkerTF, text, nh, e.getKey());

		    	for (MyOntClass cls: e.getValue())
		    		cls.setNestedList(listNested);
		    	
		    	ArrayList<MyOntClass> listCpy = new ArrayList<MyOntClass>();
		    	
		    	merge.put(e.getKey(), listCpy);
		    	
		    	for (int i = 0; i < e.getValue().size(); ++i)
		    	{
		    		MyOntClass cls = new MyOntClass(e.getValue().get(i));
		    		listCpy.add(cls);
		    		listCpy.get(i).setNestedList(listNestedWithSyn);
		    	}
		
		    }
	
			nh.CloseOutputFile();
			
			String outputStats = nh.ontStatMerge(classes);
			String outputStats2 = nh.ontStatMerge(merge);
			
			nh.openOutputFile("LingPipeStats");
			nh.writeOutput(outputStats);
			nh.writeOutput(System.getProperty("line.separator"));
			nh.writeOutput(outputStats2);
			nh.CloseOutputFile();
			
		}	
		catch (IOException e) {
			e.printStackTrace();
		}	
	         
	}

	static ArrayList<String> chunk(ExactDictionaryChunker chunker
			, String text
			, NormHM nh
			, String label) {
		
		
		ArrayList<String> listNested = new ArrayList<String>();
			
		try {
		
	    nh.writeOutput("\nChunker."
					   + " " + label
	                   + " All matches=" + chunker.returnAllMatches()
	                   + " Case sensitive=" + chunker.caseSensitive()
	                   + System.getProperty("line.separator"));
	  
	    Chunking chunking = chunker.chunk(text);
	    for (Chunk chunk : chunking.chunkSet()) {
	        int start = chunk.start();
	        int end = chunk.end();
	        String type = chunk.type();
	        double score = chunk.score();
	        String phrase = text.substring(start,end);
	        nh.writeOutput("     phrase=|" + phrase + "|"
	                       + " start=" + start
	                       + " end=" + end
	                       + " type=" + type
	                       + " score=" + score
	                       + System.getProperty("line.separator"));
	        
	        if (!listNested.contains(type) && !type.equals(label) && !phrase.equals(label))
	        	listNested.add(type);
	    }
	    
		
		}
		catch (IOException e) {
			e.printStackTrace();
		}	
	
		return listNested;
	}

}
