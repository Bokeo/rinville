package ont;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.jena.atlas.logging.Log;
import org.apache.jena.base.Sys;
import org.apache.log4j.Logger;

/**
 * NormHM is a class that doas operation on the hasmap
 * @author alerin
 *
 */
public class NormHM {
	
	static Logger log = Logger.getLogger(NormHM.class);
	
	private BufferedWriter bw = null;
	private FileWriter fw = null;
	private int maxSizeWord = 0;
	
	public NormHM()
	{
		
	}


	/**
	 * loadStopWords takes the stop words in a file and saves in a list 
	 * @return
	 */
	public ArrayList<String> loadStopWords()
	{
		ArrayList<String> stopWords = new ArrayList<String>();
		FileInputStream fis = null;
		
		try {
			fis = new FileInputStream(new File("./stopwords.txt"));
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		InputStreamReader isr = new InputStreamReader(fis);
		BufferedReader br = new BufferedReader(isr);
		
		String line = "";
		try {
			while ((line = br.readLine()) != null) 
				stopWords.add(line);
			
			br.close();
			isr.close();
			fis.close();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	 	
		return stopWords;
	}
	
	public void openOutputFile(String fileName) throws IOException
	{
		File file = new File("./" + fileName + ".txt");
		log.info("Create" + fileName + "file");
		
		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}
	
		fw = new FileWriter(file.getAbsoluteFile());
	    bw = new BufferedWriter(fw);
	    
	}
	
	public void CloseOutputFile() throws IOException
	{
		bw.close();
		fw.close();
		log.info("Done");
	}
	
	public void writeOutput(String output) throws IOException
	{
		bw.write(output);
	}
	
	public HashMap<String, MyOntClass> delStopWords(HashMap<String, MyOntClass> myClasses, String filename)
	{
		ArrayList<String> stopWords = loadStopWords();
		HashMap<String, MyOntClass> newClasses = new HashMap<String, MyOntClass>();
		
		for (Iterator<Map.Entry<String, MyOntClass>> i = myClasses.entrySet().iterator(); i.hasNext();) 
		{
			Map.Entry<String, MyOntClass> e = i.next();
			
			String label = e.getValue().getLabel();
			
			e.getValue().setLabel(takeOutStopWords(label, stopWords));
			
			ArrayList<String> newSynList = new ArrayList<String>();
			
			for (String w: e.getValue().getSynList())
			{
				newSynList.add(takeOutStopWords(w, stopWords));
			}
			
			ArrayList<String> newAncList = new ArrayList<String>();
			
			for (String w: e.getValue().getAncesList())
			{
				newAncList.add(takeOutStopWords(w, stopWords));
			}
			
			e.getValue().setSynList(newSynList);
			e.getValue().setAncList(newAncList);
			e.getValue().setFileName(filename);
			
			if (newClasses.containsKey(e.getValue().getLabel()))
			{
				System.out.println(e.getKey());
				System.out.println(e.getValue().getRealLabel());
				System.out.println(e.getValue().getLabel());
				System.out.println(newClasses.get(e.getValue().getLabel()).getRealLabel());
				System.out.println(e.getValue().getFileName());
			}
			newClasses.put(e.getValue().getLabel(), e.getValue());
		}
		return newClasses;
	}
	
	public String takeOutStopWords(String label, ArrayList<String> stopWords)
	{
		int sizeWord = 0;
		String newLabel = "";
		
		
		if (label.length() <= 4)
			return label;
			
		String[] tokens = {"(", ")", "-", "_", ",", "[", "]"};
		
		for (String t: tokens)
		{
			if (label.contains(t))
				label = label.replace(t, " ");
		}
		
		String[] words = label.split(" ");
		
		for (String w: words)
	    {
			String wordCompare = w.toLowerCase();
			
			 if (!stopWords.contains(wordCompare))
			 {
				 newLabel += wordCompare + " ";
				 ++sizeWord;
			 }
		}
	
		maxSizeWord = Math.max(sizeWord, maxSizeWord);
		
		if (newLabel.length() > 0)
			newLabel = newLabel.substring(0, newLabel.length()-1);
		
		return newLabel;
	}
	
	public void print(HashMap<String, ArrayList<MyOntClass>> allClasses, String filename)
	{
		try {
			openOutputFile(filename);	
			
			for (Iterator<Map.Entry<String, ArrayList<MyOntClass>>> k = allClasses.entrySet().iterator(); k.hasNext();)
			{
				Map.Entry<String, ArrayList<MyOntClass>> e = k.next();
				
				String output = "";
				output += "Class: " + e.getKey();
				
				for (MyOntClass moc: e.getValue())
				{	
					output += " " + moc.getId() + " " + moc.getSynList() + " [";
					
					for (String s: moc.getNestedList())
					{
						output +=  s + " [";
						int i = 0;
						
						if (allClasses.containsKey(s))
						{
							for (MyOntClass cls: allClasses.get(s))
							{
								if (i != 0)
									output += ", ";
								
								output += cls.getId();
								++i;
							}
						}			
						output += "] ";
					}
					output += "]";
				}
				output += System.getProperty("line.separator");
				writeOutput(output);
			}
			
			CloseOutputFile();
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public void WordHmap(ArrayList<HashMap<String, MyOntClass>> shmap
							, HashMap<String, ArrayList<MyOntClass>> myClasses)
	{
		for (int i = 0; i < maxSizeWord; ++i)
		{
			HashMap<String, MyOntClass> hmap = new HashMap<String, MyOntClass>();
			
			for (Iterator<Map.Entry<String, ArrayList<MyOntClass>>> k = myClasses.entrySet().iterator(); k.hasNext();) 
			{
				Map.Entry<String, ArrayList<MyOntClass>> e = k.next();	
				
				for (MyOntClass moc: e.getValue())
				{
					String label = moc.getLabel();
					String[] splitLabel = label.split(" ");
					
					if (splitLabel.length == (i+1))
						hmap.put(label, moc);
				}
			}
			
			shmap.add(hmap);
		}
	}
	
	public void nestedTerms(HashMap<String, ArrayList<MyOntClass>> allClasses)
	{
		log.info("begin nested");
		
		ArrayList<HashMap<String, MyOntClass>> shmap = new ArrayList<HashMap<String, MyOntClass>>();
		
		WordHmap(shmap, allClasses);
		
		for (int i = 0; i < maxSizeWord; ++i)
		{
			for (Iterator<Map.Entry<String, ArrayList<MyOntClass>>> k = allClasses.entrySet().iterator(); k.hasNext();)
			{
				Map.Entry<String, ArrayList<MyOntClass>> e = k.next();
				ArrayList<String> nestedList = new ArrayList<String>();
				
				for (MyOntClass moc: e.getValue())
				{
					ArrayList<String> synList = moc.getSynList();
					
					for (String syn: synList)
					{		
						String[] label = syn.split(" ");
						
						for (int j = 0; j < label.length - i; ++j)
						{
							String w = "";
					
							for (int z = 0; z <= i; ++z)
								w += label[z + j] + " ";
							
							if (w.length() > 0)
								w = w.substring(0, w.length()-1);
							
							if (shmap.get(i).containsKey(w) && !shmap.get(i).get(w).getLabel().equals(e.getKey()))
								nestedList.add(shmap.get(i).get(w).getLabel());
						
						}
					}
				}
				
				for (MyOntClass moc: e.getValue())
					moc.addListInNestedList(nestedList);
				
				String[] label = e.getKey().split(" ");
				
				for (int j = 0; j < label.length - i; ++j)
				{
					String w = "";

					for (int z = 0; z <= i; ++z)
						w += label[z + j] + " ";
	
					if (w.length() > 0)
						w = w.substring(0, w.length() - 1);
					
					if (shmap.get(i).containsKey(w) && !w.equals(e.getKey()))
					{
						for (MyOntClass moc: e.getValue())
							moc.addNestedinList(shmap.get(i).get(w).getLabel());
					}
				}			
			}
		}
	}
	
	public MyOntClass containsLevDist(String word, HashMap<String, MyOntClass> classes, int dist)
	{
		for (Iterator<Map.Entry<String, MyOntClass>> i = classes.entrySet().iterator(); i.hasNext();) 
		{
			Map.Entry<String, MyOntClass> e = i.next();
			
			if (LevenshteinDistance(word, e.getValue().getLabel(), dist) <= dist)
				return e.getValue();
		}
		
		return null;
	}
	
	public MyOntClass containsLevDistSyn(String word, HashMap<String, MyOntClass> classes, int dist)
	{
		for (Iterator<Map.Entry<String, MyOntClass>> i = classes.entrySet().iterator(); i.hasNext();) 
		{
			Map.Entry<String, MyOntClass> e = i.next();
			
			if (LevenshteinDistance(word, e.getValue().getLabel(), dist) <= dist)
				return e.getValue();
			
			for (String w: e.getValue().getSynList())
			{
				if (LevenshteinDistance(word, w, dist) <= dist)
					return e.getValue();
			}
		}
		
		return null;
	}
	
	public void createSemType(HashMap<String, SemType> allSems)
	{
		SemType s1 = new SemType("all", "all");
		SemType pheno = new SemType("phenotype", "pheno");
		SemType act = new SemType("activity", "act");
		SemType bp = new SemType("biological process", "bp");
		SemType cc = new SemType("cellular component", "cc");
		SemType mf = new SemType("molecular function", "mf");
		SemType fma = new SemType ("fundation model of anatomy", "fma");
		SemType hpo = new SemType ("human phenotype", "hpo");
		SemType mp = new SemType ("mammalian phenotype", "mp");
		SemType cl = new SemType ("cell ontology", "cl");
		SemType pato = new SemType ("pato", "pato");
		
		allSems.put(s1.getAcronym(), s1);
		allSems.put(s1.getLabel(), s1);
		
		
		
		ArrayList<SemType> children1 = new ArrayList<SemType>();
		ArrayList<SemType> children2 = new ArrayList<SemType>();
		ArrayList<SemType> children3 = new ArrayList<SemType>();
		
		children1.add(pheno);
		children1.add(act);
		
		
		children2.add(cl);
		children2.add(fma);
		children2.add(hpo);
		children2.add(mp);
		children2.add(cc);
		children2.add(pato);
		
		children3.add(bp);
		children3.add(mf);
		
		s1.setChildren(children1);	
		pheno.setChildren(children2);
		act.setChildren(children3);
		
		for (SemType s: children1)
		{
			allSems.put(s.getAcronym(), s);
			allSems.put(s.getLabel(), s);
		}
		
		for (SemType s: children2)
		{
			allSems.put(s.getAcronym(), s);
			allSems.put(s.getLabel(), s);
		}
		
		for (SemType s: children3)
		{
			allSems.put(s.getAcronym(), s);
			allSems.put(s.getLabel(), s);
		}
	}
	
	public ArrayList<ArrayList<String>> listAllSemType()
	{
		ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>(); 
	
		
		HashMap<String, SemType> semType = new HashMap<String, SemType>();
		createSemType(semType);
				
		for (Iterator<Map.Entry<String, SemType>> k = semType.entrySet().iterator(); k.hasNext();)
		{		
			Map.Entry<String, SemType> e = k.next();
			
			ArrayList<String> listSems = new ArrayList<String>();
			
			listSems.add(e.getValue().getAcronym());
			getSemTypeChildren(e.getValue(), listSems);
			
			if (!list.contains(listSems))
				list.add(listSems);
		
		}
		
		return list;
	}
	
	public ArrayList<ArrayList<String>> listAllSemChildren()
	{
		ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>(); 
	
		HashMap<String, SemType> semType = new HashMap<String, SemType>();
		createSemType(semType);
			
		for (Iterator<Map.Entry<String, SemType>> k = semType.entrySet().iterator(); k.hasNext();)
		{		
			Map.Entry<String, SemType> e = k.next();
			
			ArrayList<String> listSems = new ArrayList<String>();
			
			
			if (e.getValue().getChildren().isEmpty())
				listSems.add(e.getValue().getAcronym());
			
			if (!list.contains(listSems) && !listSems.isEmpty())
				list.add(listSems);
		
		}
		
		return list;
	}
	
	public void getSemTypeChildren(SemType sem, ArrayList<String> semTypeList)
    {
    	for (SemType s: sem.getChildren())
    	{
    		if (!semTypeList.contains(s.getAcronym()))
    			semTypeList.add(s.getAcronym());
    		
    		getSemTypeChildren(s, semTypeList);
    	}
    }
	
	public void addSemanticType(HashMap<String, ArrayList<MyOntClass>> merge, HashMap<String, SemType> allSems)
	{	
		for (Iterator<Map.Entry<String, ArrayList<MyOntClass>>> k = merge.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, ArrayList<MyOntClass>> e = k.next();
			
			for (MyOntClass cls: e.getValue())
			{
				String ont = cls.getId().replaceAll("[^a-zA-Z]", "").toLowerCase();
				String file = cls.getFileName();
				
				String fileName =  (file.split("/")[file.split("/").length - 1]).split("\\.")[0];
				
				
				if (fileName.equals("go") || ont.equals("go"))
				{
					if (cls.getOboNamespace() != null)
					{	
						if (cls.getOboNamespace().equals("biological_process"))
							cls.setSemType(allSems.get("bp"));
						else if (cls.getOboNamespace().equals("cellular_component"))
							cls.setSemType(allSems.get("cc"));
						else if (cls.getOboNamespace().equals("molecular_function"))
							cls.setSemType(allSems.get("mf"));
					}
					else
					{					
						if (getOboSemType(cls, allSems, merge).equals("biological_process"))
							cls.setSemType(allSems.get("bp"));
						else if (getOboSemType(cls, allSems, merge).equals("cellular_component"))
							cls.setSemType(allSems.get("cc"));
						else if (getOboSemType(cls, allSems, merge).equals("molecular_function"))
							cls.setSemType(allSems.get("mf"));
					}
				}
				else
				{
					if (allSems.containsKey(fileName))
						cls.setSemType(allSems.get(fileName));
					else
					{
						log.info("didn't find semantic type");
						cls.setSemType(allSems.get("all"));
					}
				}
				
				
				if (cls.getSemType() == null)
				{
					log.info("Semantic type null: " + e.getKey() + "   " + ont + "  " + file);
					cls.setSemType(allSems.get("all"));
				}
			}
		}
	}
	
	public String getOboSemType(MyOntClass cls, HashMap<String, SemType> allSems, HashMap<String, ArrayList<MyOntClass>> merge)
	{
		for (MyOntClass moc: merge.get(cls.getLabel()))
		{	
			String file = moc.getFileName();
			String fileName =  (file.split("/")[file.split("/").length - 1]).split("\\.")[0];
			
			if (fileName.equals("go"))
				return moc.getOboNamespace();
		}
		
		return "";
	}
	
	public boolean isAncestor(MyOntClass cls
							, String label
							, HashMap<String, ArrayList<MyOntClass>> merge
							, String ontName)
	{	
		if (cls.getLabel().toLowerCase().equals(label))
			return true;
		
		for (MyOntClass myCls: merge.get(cls.getLabel()))
		{
			ArrayList<String> ancList = myCls.getAncesList();
			
			String file = myCls.getFileName();
			String fileName =  (file.split("/")[file.split("/").length - 1]).split("\\.")[0];
		
			if (fileName.equals(ontName))
			{	
				for (int i = 0; i < ancList.size(); ++i)
				{
					if (merge.containsKey(ancList.get(i)))
					{
						for (MyOntClass moc: merge.get(ancList.get(i)))
						{			
							String ont = moc.getId().replaceAll("[^a-zA-Z]", "").toLowerCase();
							String file2 = moc.getFileName();
							String fileName2 =  (file2.split("/")[file2.split("/").length - 1]).split("\\.")[0];
							
							if (fileName2.equals(ontName))
							{
								if (isAncestor(moc, label, merge, ontName));
									return true;
							}
						
						}	
					}
					
				}
			}
		}
		
		return false;
	}
	
	public void mergeTerms(ArrayList<HashMap<String, MyOntClass>> allClasses,
						HashMap<String, ArrayList<MyOntClass>> merge, HashMap<String, ArrayList<MyOntClass>> sharedTerms)
	{
		
		merge.putAll(sharedTerms);
		
		for (int i = 0; i < allClasses.size(); ++i)
		{
			for (Iterator<Map.Entry<String, MyOntClass>> k = allClasses.get(i).entrySet().iterator(); k.hasNext();)
			{
				Map.Entry<String, MyOntClass> e = k.next();
				
				if (!merge.containsKey(e.getValue().getLabel()))
				{
					ArrayList<MyOntClass> newClass = new ArrayList<MyOntClass>();
					
					newClass.add(e.getValue());
					
					merge.put(e.getValue().getLabel(), newClass);
				}
//				else if (!merge.containsKey(e.getValue().getLabel()))
//					merge.put(e.getValue().getLabel(), sharedTerms.get(e.getValue().getLabel()));
				
			}
		}
	}
	
	public void searchSharedAndSynTerms(ArrayList<HashMap<String, MyOntClass>> allClasses
			, HashMap<String, ArrayList<MyOntClass>> sharedTerms
			, HashMap<String, ArrayList<MyOntClass>> synTerms)
	{
		for (int i = 0; i < allClasses.size(); ++i)
		{
			for (Iterator<Map.Entry<String, MyOntClass>> k = allClasses.get(i).entrySet().iterator(); k.hasNext();)
			{
				Map.Entry<String, MyOntClass> e = k.next();
				
				for (int j = 0; j < allClasses.size(); ++j)
				{
					String label = e.getValue().getLabel();
					
					if (allClasses.get(j).containsKey(label))
					{
						if (!allClasses.get(j).get(label).getFileName().equals(e.getValue().getFileName()))
						{
							if (sharedTerms.containsKey(label))
							{
								ArrayList<MyOntClass> newSharedTerms = sharedTerms.get(label);
										
								if (!newSharedTerms.contains(e.getValue()))
									newSharedTerms.add(e.getValue());
								
								if (!newSharedTerms.contains(allClasses.get(j).get(label)))
									newSharedTerms.add(allClasses.get(j).get(label));
							}
							else
							{
								ArrayList<MyOntClass> newSharedTerms = new ArrayList<MyOntClass>();
								
								newSharedTerms.add(e.getValue());
								newSharedTerms.add(allClasses.get(j).get(label));
								
								sharedTerms.put(label, newSharedTerms);
							}
						}
					}
				}
				
				ArrayList<String> syns = e.getValue().getSynList();
				
				for (String syn: syns)
				{
					for (int j = 0; j < allClasses.size(); ++j)
					{
						if (allClasses.get(j).containsKey(syn))
						{	
							if (synTerms.containsKey(e.getValue().getLabel()))
							{
								ArrayList<MyOntClass> newSynTerms = synTerms.get(e.getValue().getLabel());
								
								if (!newSynTerms.contains(e.getValue()))
									newSynTerms.add(e.getValue());
								
								if (!newSynTerms.contains(allClasses.get(j).get(syn)))
									newSynTerms.add(allClasses.get(j).get(syn));
							}
							else
							{
								ArrayList<MyOntClass> newSynTerms = new ArrayList<MyOntClass>();								
								newSynTerms.add(e.getValue());
								newSynTerms.add(allClasses.get(j).get(syn));
								if (!e.getValue().equals(allClasses.get(j).get(syn)))
									synTerms.put(e.getValue().getLabel(), newSynTerms);
							}

						}
					}
				}
 			}
		}
	}
	
	public int LevenshteinDistance (String lhs, String rhs, int dist)
	{                          
	    int len0 = lhs.length() + 1;                                                     
	    int len1 = rhs.length() + 1;                                                     
	                                                                                    
	    // the array of distances                                                       
	    int[] cost = new int[len0];                                                     
	    int[] newcost = new int[len0];                                                  
	                                                                                    
	    // initial cost of skipping prefix in String s0                                 
	    for (int i = 0; i < len0; i++) cost[i] = i;                                     
	                                                                                    
	    // dynamically computing the array of distances                                  
	                                                                                    
	    // transformation cost for each letter in s1                                    
	    for (int j = 1; j < len1; j++) {                                                
	        // initial cost of skipping prefix in String s1                             
	        newcost[0] = j;                                                             
	                               
	        int minLineCost = j;
	        
	        // transformation cost for each letter in s0                                
	        for(int i = 1; i < len0; i++) {                                             
	            // matching current letters in both strings                             
	            int match = (lhs.charAt(i - 1) == rhs.charAt(j - 1)) ? 0 : 1;             
	                                                                                    
	            // computing cost for each transformation                               
	            int cost_replace = cost[i - 1] + match;                                 
	            int cost_insert  = cost[i] + 1;                                         
	            int cost_delete  = newcost[i - 1] + 1;                                  
	                                                                                    
	            // keep minimum cost                                                    
	            newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
	            
	            minLineCost = Math.min(minLineCost, newcost[i]);
	        }                                                                           
	                                                                           
	        // swap cost/newcost arrays                                                 
	        int[] swap = cost; cost = newcost; newcost = swap;             
	        
	        if (minLineCost > dist)
	        	return minLineCost;
	    }                                                                               
	                                                                                    
	    // the distance is the cost for transforming all letters in both strings        
	    return cost[len0 - 1];                                                          
	}


	public MyOntClass searchTerm(HashMap<String, ArrayList<MyOntClass>> hm, String term, String id)
	{
		if (hm.containsKey(term))
		{
			for (MyOntClass cls: hm.get(term))
			{
				if (cls.getId().equals(id))
					return cls;
			}
		}
		
		
		return null;
	}
	
	public String ontStatMerge(HashMap<String, ArrayList<MyOntClass>> merge)
	{
		String output = "";
		
		int totalNestedTerms = 0;
		int totalNestedTerms2 = 0;
		int totalTerms = 0;
		int total = 0;
		int totalNestedTermsDiffOnt = 0;
		int totalNestedTermsDiffFile = 0;
		int maxNestedTerms = 0;
		int minNestedTerms = merge.size();
		int termsWithDiffId = 0;
		
		
		HashMap<Integer, Integer> nbrTermsbyMerge = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> sizeWord = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> sizeTerms = new HashMap<Integer, Integer>();
		
		HashMap<String, Integer> nbrTermsByOnt = new HashMap<String, Integer>();
		HashMap<String, Integer> nbrTermsByFile = new HashMap<String, Integer>();
		
		for (Iterator<Map.Entry<String, ArrayList<MyOntClass>>> k = merge.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, ArrayList<MyOntClass>> e = k.next();
			
			ArrayList<MyOntClass> clsList = e.getValue();
			
			ArrayList<String> idList = new ArrayList<String>();
			
			for (int i = 0; i < clsList.size(); ++i)
			{
				MyOntClass cls = clsList.get(i);
				
				if (cls.getNestedList().size() > 0)
					++totalTerms;
				
				if (!idList.contains(cls.getId()))
					++termsWithDiffId;
				
				idList.add(cls.getId());
				
				maxNestedTerms = Math.max(maxNestedTerms, cls.getNestedList().size());
				minNestedTerms = Math.min(minNestedTerms, cls.getNestedList().size());
				
				int lengthTerms = cls.getLabel().split(" ").length;
				
				if (sizeTerms.containsKey(lengthTerms))
					sizeTerms.replace(lengthTerms, sizeTerms.get(lengthTerms) + 1);
				else
					sizeTerms.put(lengthTerms, 1);
			
				if (nbrTermsbyMerge.containsKey(cls.getNestedList().size()))
					nbrTermsbyMerge.replace(cls.getNestedList().size(), nbrTermsbyMerge.get(cls.getNestedList().size()) + 1);
				else
					nbrTermsbyMerge.put(cls.getNestedList().size(), 1);
				
				for (int j = 0; j < cls.getNestedList().size(); ++j)
				{
					++totalNestedTerms;
					
					
					int length = cls.getNestedList().get(j).split(" ").length;
					if (sizeWord.containsKey(length))
						sizeWord.replace(length, sizeWord.get(length) + 1);
					else
						sizeWord.put(length, 1);
					
					if (merge.containsKey(cls.getNestedList().get(j)))
					{
						
						boolean id = false;
						boolean file = false;
						
						for (MyOntClass clstmp: merge.get(cls.getNestedList().get(j)))
						{
							++totalNestedTerms2;
						
							if (!id && !clstmp.getId().replaceAll("[0-9]", "").equals(cls.getId().replaceAll("[0-9]", "")))
							{
								++totalNestedTermsDiffOnt;
//								id = true;
							}
							
							if (!file && !clstmp.getFileName().equals(cls.getFileName()))
							{
								++totalNestedTermsDiffFile;
//								file = true;
							}
							
							String idCls = clstmp.getId().replaceAll("[0-9]", "");
							
							if (nbrTermsByOnt.containsKey(idCls))
								nbrTermsByOnt.replace(idCls, nbrTermsByOnt.get(idCls) + 1);
							else
								nbrTermsByOnt.put(idCls, 1);
							
							if (nbrTermsByFile.containsKey(clstmp.getFileName()))
								nbrTermsByFile.replace(clstmp.getFileName(), nbrTermsByFile.get(clstmp.getFileName()) + 1);
							else
								nbrTermsByFile.put(clstmp.getFileName(), 1);
							
//							if (id && file)
//								break;
						}
					}
				}			
				++total;
				
			}
		}
		
		float meanNestedbyTerms = (float) totalNestedTerms / total;
		float meanNestedbyTermsDiffOnt = ((float) totalNestedTermsDiffOnt / totalNestedTerms2) * 100;
		float meanNestedbyTermsDiffFile = ((float) totalNestedTermsDiffFile / totalNestedTerms2) * 100;
		
		output += "The total of terms with a different label: " + merge.size() + System.getProperty("line.separator");
		output += "The total of terms: " + total + System.getProperty("line.separator");
		output += "The amount of terms with different ids: " + termsWithDiffId + System.getProperty("line.separator");
		output += "Nested Terms:" + System.getProperty("line.separator");
		output += "\t" + "The amount of terms who have nested terms: " + totalTerms + System.getProperty("line.separator");
		output += "\t" + "The total amount of nested terms: " + totalNestedTerms2 + System.getProperty("line.separator");
		output += "\t" + "Minimum amount of nested terms: " + minNestedTerms + System.getProperty("line.separator");
		output += "\t" + "Maximum nested terms: " + maxNestedTerms + System.getProperty("line.separator");
		
		output += "\tThe amount of terms with: ";
		output += System.getProperty("line.separator");
		for (Iterator<Map.Entry<Integer, Integer>> k = nbrTermsbyMerge.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<Integer, Integer> e = k.next();
			
			float pourcentageNbrTerms = ((float) e.getValue() / total) * 100;
			
			output += "\t\t" + e.getKey() + " nested terms: " + e.getValue() + "\t\t" + pourcentageNbrTerms + "%";
			output += System.getProperty("line.separator");
			
		}
		output += System.getProperty("line.separator");
		output += System.getProperty("line.separator");
		output += "\tThe amount of nested terms who have a length of: ";
		output += System.getProperty("line.separator");
		
		for (Iterator<Map.Entry<Integer, Integer>> k = sizeWord.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<Integer, Integer> e = k.next();
			
			float pourcentageSizeWord = ((float) e.getValue() / totalNestedTerms) * 100;
			
			output += "\t\t" + e.getKey() + " words: " + e.getValue() + "\t\t" + pourcentageSizeWord + "%";
			output += System.getProperty("line.separator");
		}
		output += System.getProperty("line.separator");
		output += System.getProperty("line.separator");
		
		output += "\t" + "Mean of nested terms for a unique term: " + meanNestedbyTerms + System.getProperty("line.separator"); 
		output += "\t" + "The amount of nested terms with a different ontology than its term: " + totalNestedTermsDiffOnt + System.getProperty("line.separator");
		output += "\t" + "The % of nested terms with a different ontology of its term: " + meanNestedbyTermsDiffOnt + "%" + System.getProperty("line.separator"); 
		output += "\t" + "The amount of nested terms with a different file than its term: " + totalNestedTermsDiffFile + System.getProperty("line.separator");
		output += "\t" + "The % nested terms by terms with a different file than its term: " + meanNestedbyTermsDiffFile + "%" + System.getProperty("line.separator");
		output += System.getProperty("line.separator");

		output += "\tThe repartition of the size of terms accross the different ontologies"
				+ System.getProperty("line.separator");
		
		for (Iterator<Map.Entry<Integer, Integer>> k = sizeTerms.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<Integer, Integer> e = k.next();
			
			float pourcTerms = ((float) e.getValue() / total) * 100;
			
			output += "\t\t" + e.getKey() +  " words: " + e.getValue() + "\t\t" + pourcTerms + "%";
			output += System.getProperty("line.separator");
		}
		
		output += System.getProperty("line.separator");
		output += System.getProperty("line.separator");
		
		output += "\tThe repartition of the nested terms accross the different ontologies"
				+ System.getProperty("line.separator");
		
		
		nbrTermsByOnt = (HashMap<String, Integer>) MapUtil.sortByValue(nbrTermsByOnt);
		for (Iterator<Map.Entry<String, Integer>> k = nbrTermsByOnt.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, Integer> e = k.next();
			
			float pourcTerms = ((float) e.getValue() / totalNestedTerms2) * 100;
			
			output += "\t\t" + e.getKey() + " " + e.getValue() + "\t\t" + pourcTerms + "%"
					+ System.getProperty("line.separator");
		}
		
		output += System.getProperty("line.separator");
		
		output += "\tThe repartition of the nested terms accross the different files:"
				+ System.getProperty("line.separator");
		
		nbrTermsByFile = (HashMap<String, Integer>) MapUtil.sortByValue(nbrTermsByFile);
		for (Iterator<Map.Entry<String, Integer>> k = nbrTermsByFile.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, Integer> e = k.next();
			
			float pourcTerms = ((float) e.getValue() / totalNestedTerms2) * 100;
			
			output += "\t\t" + e.getKey().split("/")[e.getKey().split("/").length - 1] + " " + e.getValue() + "\t\t" + pourcTerms + "%"
					+ System.getProperty("line.separator");
			
		}
		
		output += System.getProperty("line.separator");
		
		return output;
	}
	
	public String ontStatSynTerms(HashMap<String, ArrayList<MyOntClass>> synTerms)
	{
		String output = "The amount of terms with shared labels via a synonym: " + synTerms.size() + System.getProperty("line.separator");
		
		int synWithDiffId = 0;
		int synWithDiffFile = 0;
		int nbrSynTerms = 0;
		int minSynTerms = 10000;
		int maxSynTerms = 0;
		
		HashMap<Integer, Integer> nbrTermsbySyn = new HashMap<Integer, Integer>(); 
		HashMap<String, Integer> nbrTermsById = new HashMap<String, Integer>();
		HashMap<String, Integer> nbrTermsFromFile = new HashMap<String, Integer>(); 
		
		for (Iterator<Map.Entry<String, ArrayList<MyOntClass>>> k = synTerms.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, ArrayList<MyOntClass>> e = k.next();
			
			ArrayList<MyOntClass> clsList = e.getValue();
			
			ArrayList<String> idList = new ArrayList<String>();
			ArrayList<String> fileList = new ArrayList<String>();
			
			nbrSynTerms += clsList.size();
			
			minSynTerms = Math.min(minSynTerms, clsList.size());
			maxSynTerms = Math.max(maxSynTerms, clsList.size());
			
			if (nbrTermsbySyn.containsKey(clsList.size()))
				nbrTermsbySyn.replace(clsList.size(), nbrTermsbySyn.get(clsList.size()) + 1);
			else
				nbrTermsbySyn.put(clsList.size(), 1);
			
			
			for (int i = 0; i < clsList.size(); ++i)
			{
				MyOntClass cls = clsList.get(i);
								
				if (!idList.contains(cls.getId()))
					++synWithDiffId;
				
				if (!fileList.contains(cls.getFileName()))
					++synWithDiffFile;
			
				fileList.add(cls.getFileName());
				idList.add(cls.getId());
				
				String id = cls.getId().replaceAll("[0-9]", "");
				
				if (nbrTermsById.containsKey(id))
					nbrTermsById.replace(id, nbrTermsById.get(id) + 1);
				else
					nbrTermsById.put(id, 1);
				
				if (nbrTermsFromFile.containsKey(cls.getFileName()))
					nbrTermsFromFile.replace(cls.getFileName(), nbrTermsFromFile.get(cls.getFileName()) + 1);
				else
					nbrTermsFromFile.put(cls.getFileName(), 1);

			}
		}
		
		float meanSynWithDiffId = ((float) synWithDiffId / nbrSynTerms) * 100;
		float meanSynWithDiffFile = ((float) synWithDiffFile / nbrSynTerms) * 100;
		float meanSynTerms = (float) nbrSynTerms / synTerms.size();
		
		output += "\tThe amount of terms with the same label: ";
		output += System.getProperty("line.separator");
		
		for (Iterator<Map.Entry<Integer, Integer>> k = nbrTermsbySyn.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<Integer, Integer> e = k.next();
			output += "\t\t" + e.getValue() + " for the synonyms of a term and " + (e.getKey() - 1) + " terms";
//			output += "\t\t" + e.getKey() + " : " + e.getValue();
			output += System.getProperty("line.separator");
		}
		output += System.getProperty("line.separator");
		
		output += "\t" + "Minimum amount of synonym terms: " + minSynTerms + System.getProperty("line.separator");
		output += "\t" + "Maximum amoutn of synonym terms: " + maxSynTerms + System.getProperty("line.separator");
		output += "\t" + "The total amount of synonym terms: " + nbrSynTerms + System.getProperty("line.separator");
		output += "\t" + "Mean of synonym terms for a unique term: " + meanSynTerms + System.getProperty("line.separator");
		output += "\t" + "The amount of synonym terms with different ids: " + synWithDiffId + System.getProperty("line.separator"); 
		output += "\t" + "The % synonym terms with different ids: " + meanSynWithDiffId + "%" + System.getProperty("line.separator");
		output += "\t" + "The amount of synonym terms with different files: " + synWithDiffFile + System.getProperty("line.separator"); 
		output += "\t" + "The % synonym terms with different files: " + meanSynWithDiffFile + "%" + System.getProperty("line.separator"); 
		output += System.getProperty("line.separator");
		
		output += "\tThe repartition of the terms accross the different ontologies."
				+ System.getProperty("line.separator");
		
		
		nbrTermsById = (HashMap<String, Integer>) MapUtil.sortByValue(nbrTermsById);
		for (Iterator<Map.Entry<String, Integer>> k = nbrTermsById.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, Integer> e = k.next();
			
			float pourcTerms = ((float) e.getValue() / nbrSynTerms) * 100;
			
			output += "\t\t" + e.getKey() + ": " + e.getValue() + "\t\t" + pourcTerms + "%"
					+ System.getProperty("line.separator");
		}
		
		output += System.getProperty("line.separator");
		
		output += "\tThe repartition of the terms accross the different files:"
				+ System.getProperty("line.separator");
		
		
		nbrTermsFromFile = (HashMap<String, Integer>) MapUtil.sortByValue(nbrTermsFromFile);
		for (Iterator<Map.Entry<String, Integer>> k = nbrTermsFromFile.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, Integer> e = k.next();
			
			float pourcTerms = ((float) e.getValue() / nbrSynTerms) * 100;
			
			output += "\t\t" + e.getKey().split("/")[e.getKey().split("/").length - 1] + ": " + e.getValue() + "\t\t" + pourcTerms + "%"
					+ System.getProperty("line.separator");
		}
		
		output += System.getProperty("line.separator");
		
		return output;
	}
	
	public String ontStatSharedTerms(HashMap<String, ArrayList<MyOntClass>> sharedTerms)
	{
		String output = "The amount of terms with shared labels: " + sharedTerms.size() + System.getProperty("line.separator");
		
		int sharedWithDiffId = 0;
		int nbrSharedTerms = 0;
		int minSharedTerms = 1000;
		int maxSharedTerms = 0;
		
		HashMap<Integer, Integer> nbrTermsbyShared = new HashMap<Integer, Integer>(); 
		HashMap<String, Integer> nbrSharedTermsById = new HashMap<String, Integer>();
		HashMap<String, Integer> nbrSharedTermsFromFile = new HashMap<String, Integer>();
		
		for (Iterator<Map.Entry<String, ArrayList<MyOntClass>>> k = sharedTerms.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, ArrayList<MyOntClass>> e = k.next();
			
			ArrayList<MyOntClass> clsList = e.getValue();
			
			ArrayList<String> idList = new ArrayList<String>();
		
			
			nbrSharedTerms += clsList.size();
			
			minSharedTerms = Math.min(minSharedTerms, clsList.size());
			maxSharedTerms = Math.max(maxSharedTerms, clsList.size());
			
			if (nbrTermsbyShared.containsKey(clsList.size()))
				nbrTermsbyShared.replace(clsList.size(), nbrTermsbyShared.get(clsList.size()) + 1);
			else
				nbrTermsbyShared.put(clsList.size(), 1);
			
			
			for (int i = 0; i < clsList.size(); ++i)
			{
				MyOntClass cls = clsList.get(i);
								
				if (!idList.contains(cls.getId()))
					++sharedWithDiffId;
				
				String id = cls.getId().replaceAll("[0-9]", "");
				
				if (nbrSharedTermsById.containsKey(id))
					nbrSharedTermsById.replace(id, nbrSharedTermsById.get(id) + 1);
				else
					nbrSharedTermsById.put(id, 1);
				
				if (nbrSharedTermsFromFile.containsKey(cls.getFileName()))
					nbrSharedTermsFromFile.replace(cls.getFileName(), nbrSharedTermsFromFile.get(cls.getFileName()) + 1);
				else
					nbrSharedTermsFromFile.put(cls.getFileName(), 1);
				
				
				idList.add(cls.getId());

			}
		}
		
		float meanSharedWithDiffId = ((float) sharedWithDiffId / nbrSharedTerms) * 100;
		float meanSharedTerms = (float) nbrSharedTerms / sharedTerms.size();
		
		output += "\tThe amount of shared terms for a term with: ";
		
		output += System.getProperty("line.separator");
		for (Iterator<Map.Entry<Integer, Integer>> k = nbrTermsbyShared.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<Integer, Integer> e = k.next();
			
			output += "\t\t" + e.getKey() + " shared terms: " + e.getValue();
			output += System.getProperty("line.separator");
		}
		output += System.getProperty("line.separator");
		
		output += "\t" + "Minimum amount of shared terms: " + minSharedTerms + System.getProperty("line.separator");
		output += "\t" + "Maximum amount of shared terms: " + maxSharedTerms + System.getProperty("line.separator");
		output += "\t" + "The total amount of shared terms: " + nbrSharedTerms + System.getProperty("line.separator");
		output += "\t" + "Mean of shared terms for an unique terms: " + meanSharedTerms + System.getProperty("line.separator");
		output += "\t" + "The amount of shared terms with different ids: " + sharedWithDiffId + System.getProperty("line.separator"); 
		output += "\t" + "The % of shared terms with different ids: " + meanSharedWithDiffId + "%" + System.getProperty("line.separator"); 
		output += System.getProperty("line.separator");
		
		
		output += "\tThe repartition of the shared terms accross the different ontologies:"
				+ System.getProperty("line.separator");	
		
		nbrSharedTermsById = (HashMap<String, Integer>) MapUtil.sortByValue(nbrSharedTermsById);
		for (Iterator<Map.Entry<String, Integer>> k = nbrSharedTermsById.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, Integer> e = k.next();
			
			float pourcNbrSharedTermsById = ((float) e.getValue() / nbrSharedTerms) * 100;
			
			output += "\t\t" + e.getKey() + ": " + e.getValue() + "\t\t" + pourcNbrSharedTermsById + "%"
					+ System.getProperty("line.separator");
		}
		
		output += System.getProperty("line.separator");
		
		output += "\tThe repartition of the shared terms accross the different files:"
				+ System.getProperty("line.separator");	
		
		nbrSharedTermsFromFile = (HashMap<String, Integer>) MapUtil.sortByValue(nbrSharedTermsFromFile);
 		for (Iterator<Map.Entry<String, Integer>> k = nbrSharedTermsFromFile.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, Integer> e = k.next();
			
			float pourcNbrSharedTermsFromFile = ((float) e.getValue() / nbrSharedTerms) * 100;
			
			output += "\t\t" + e.getKey().split("/")[e.getKey().split("/").length - 1] + ": " + e.getValue() + "\t\t" + pourcNbrSharedTermsFromFile + "%"
					+ System.getProperty("line.separator");
		}
		
		output += System.getProperty("line.separator");
		
		return output;
	}
	
	public String ontStatFile(HashMap<String, MyOntClass> classes, HashMap<String, Integer> nbrTotalId)
	{
		HashMap<String, Integer> nbrIds = new HashMap<String, Integer>();
		String output = "";
		
		for (Iterator<Map.Entry<String, MyOntClass>> k = classes.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, MyOntClass> e = k.next();
			
			MyOntClass cls = e.getValue();
					
			String id = cls.getId().replaceAll("[0-9]", "");
			
			if (nbrIds.containsKey(id))
				nbrIds.replace(id, nbrIds.get(id) + 1);
			else
				nbrIds.put(id, 1);
					
		}
		
	
		nbrIds = (HashMap<String, Integer>) MapUtil.sortByValue(nbrIds);

		output += "\tThe amount of terms for each Id: " + System.getProperty("line.separator");
		for (Iterator<Map.Entry<String, Integer>> k = nbrIds.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, Integer> e = k.next();
			
			if (nbrTotalId.containsKey(e.getKey()))
				nbrTotalId.replace(e.getKey(), nbrTotalId.get(e.getKey()) + e.getValue());
			else
				nbrTotalId.put(e.getKey(), e.getValue());
			
			float pourcnbrIds = ((float) e.getValue() / classes.size()) * 100;
			output += "\t\t" + e.getKey() + ": " + e.getValue() + "\t\t" + pourcnbrIds + "%"
					+ System.getProperty("line.separator");
			
		}
		
		return output;
	}
	
	public void ontStats(ArrayList<HashMap<String, MyOntClass>> allClasses
			, ArrayList<String> filename
			, HashMap<String, ArrayList<MyOntClass>> sharedTerms
			, HashMap<String, ArrayList<MyOntClass>> synTerms
			, HashMap<String, ArrayList<MyOntClass>> merge)
	{
		String output = "";
		int total = 0;
		
		HashMap<String, Integer> nbrTotalId = new HashMap<String, Integer>();
		
		for (int i = 0; i < allClasses.size(); ++i)
		{
			output += filename.get(i) + " :" + allClasses.get(i).size() + " terms"
					+ System.getProperty("line.separator");
			output += ontStatFile(allClasses.get(i), nbrTotalId);
			
			total += allClasses.get(i).size();
		}
		
		output += System.getProperty("line.separator");
		output += "The repartiton of the ids across all the ontologies:";
		output += System.getProperty("line.separator");
		
		
		nbrTotalId = (HashMap<String, Integer>) MapUtil.sortByValue(nbrTotalId);
		for (Iterator<Map.Entry<String, Integer>> k = nbrTotalId.entrySet().iterator(); k.hasNext();)
		{
			
			Map.Entry<String, Integer> e = k.next();
			float pourc = ((float) e.getValue() / total) * 100;
			
			output += "\t" + e.getKey() + ": " + e.getValue() + "\t\t" + pourc + "%"
					+ System.getProperty("line.separator");
		}
		
		output += System.getProperty("line.separator");
		
		output += ontStatSharedTerms(sharedTerms);
		output += ontStatSynTerms(synTerms);
		output += ontStatMerge(merge);		 
		
		try {
		
			openOutputFile("StatOutput");
			writeOutput(output);
			CloseOutputFile();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}
}
