package ont;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.Chunking;
import com.aliasi.dict.DictionaryEntry;
import com.aliasi.dict.ExactDictionaryChunker;
import com.aliasi.dict.MapDictionary;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;


public class NormLingPipe {
	
	static Logger log = Logger.getLogger(NormLingPipe.class);
	static final double CHUNK_SCORE = 1.0;
	
	public NormLingPipe()
	{
		
	}
	
	public void nestedTermsTest(HashMap<String, ArrayList<MyOntClass>> merge)
	{
		NormHM nh = new NormHM();
		
		MapDictionary<String> dictionary = new MapDictionary<String>();
		
		StringBuilder sb = new StringBuilder();		
		sb.append(System.getProperty("line.separator"));
		
		for (Iterator<Map.Entry<String, ArrayList<MyOntClass>>> k = merge.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, ArrayList<MyOntClass>> e = k.next();
			
			ArrayList<String> semTypeList = new ArrayList<String>();
						
			
			for (MyOntClass cls: e.getValue())
			{
				if (!semTypeList.contains(cls.getSemType().getAcronym()))
					semTypeList.add(cls.getSemType().getAcronym());
			}
			
			DictionaryEntry<String> de = new DictionaryEntry<String>(e.getKey(), semTypeList.toString(), CHUNK_SCORE);
			dictionary.addEntry(de);
			
			sb.append(e.getKey()); 
			sb.append(System.getProperty("line.separator"));
		
		}
		
		ExactDictionaryChunker dictionaryChunker = new ExactDictionaryChunker(dictionary
													, IndoEuropeanTokenizerFactory.INSTANCE
													, true
													, false);

		Chunking chunking = chunk(dictionaryChunker, sb.toString().substring(0, sb.length()));
		
		
		boolean keepNestedTerms = true;
		
		if (keepNestedTerms)
			printKeepAllNestedTerms(chunking, sb);
		else
			printNestedTerms(chunking, sb);
	}
	
	
	public void printKeepAllNestedTerms(Chunking chunking, StringBuilder sb)
	{
		NormHM nh = new NormHM();
		
		StringBuilder nsb = new StringBuilder();
		int realIndex = 0;
		int modRealIndex = 10000;
		ArrayList<Integer> realIndexList = new ArrayList<Integer>(Collections.nCopies(modRealIndex, 0));
		int lastStart = -1;
		int lastEnd = 0;
		int lastSubStart = -1;
		int lastSubEnd = 0;
		
		for (Chunk chunk: chunking.chunkSet())
		{
			int start = chunk.start();
            int end = chunk.end();
            String type = chunk.type();
            String tag = "SemType=" + type + " ";
            //String tag = "";
            int lengthType = tag.length();
            double score = chunk.score();
            String phrase = sb.substring(start,end);      
        
            if (start < lastStart || (start == lastStart && end > lastEnd))
            {
            	
            	if (nsb.substring(start + realIndexList.get((start - 1) % modRealIndex),
            			start + 1 + realIndexList.get((start - 1) % modRealIndex)).equals("<"))
            	{
            		nsb = nsb.replace(start + realIndexList.get((start - 1) % modRealIndex)
            				, start + realIndexList.get(start % modRealIndex) + 1
            				, "<" + tag + nsb.substring(start + realIndexList.get((start - 1) % modRealIndex)
            						, start + realIndexList.get(start % modRealIndex) + 1));
            	}
            	else
            		nsb = nsb.replace(start + realIndexList.get(start % modRealIndex)
        				, start + realIndexList.get(start % modRealIndex) + 1
        				, "<" + tag + nsb.substring(start + realIndexList.get(start % modRealIndex)
        						, start + realIndexList.get(start % modRealIndex) + 1));     
        	
        		nsb.append(sb.substring(lastEnd, end) + ">");
        	
            	for (int i = start; i < lastEnd; ++i)
            		realIndexList.set(i % modRealIndex, realIndexList.get(i % modRealIndex) + 1 + lengthType);
            	
            	realIndexList.set(lastEnd % modRealIndex, realIndexList.get(lastEnd % modRealIndex) + 1 + lengthType); 
            	
            	for (int i = lastEnd + 1; i < end; ++i)
            		realIndexList.set(i % modRealIndex, realIndexList.get(lastEnd % modRealIndex));
            	
            	realIndexList.set(end % modRealIndex, realIndexList.get((end - 1) % modRealIndex) + 1);	
            	
            	lastStart = start;
                lastEnd = end;
            }
            else if (start > lastEnd)
            {   	
            	nsb.append(sb.substring(lastEnd, start));
            	nsb.append("<" + tag + sb.substring(start, end) + ">");
            	
            	realIndex = realIndexList.get(lastEnd % modRealIndex);
            	
            	for (int i = lastEnd + 1; i < start; ++i)
            		realIndexList.set(i % modRealIndex, realIndex);        	
            	
            	for (int i = start; i < end; ++i)
            		realIndexList.set(i % modRealIndex, realIndex + 1 + lengthType);
            	
            	realIndexList.set(end % modRealIndex, realIndex + 2 + lengthType);
            	
            	lastStart = start;
                lastEnd = end;
            }
            else if (start > lastStart && end <= lastEnd)
            { 	
            	int diff = realIndexList.get((start - 1) % modRealIndex) - realIndexList.get(start % modRealIndex);
            	int diff2 = realIndexList.get(end % modRealIndex) - realIndexList.get((end - 1) % modRealIndex);
            	
            	String str = nsb.substring(start + realIndexList.get(start % modRealIndex) + diff
            			, end + realIndexList.get((end - 1) % modRealIndex));
            	
            	int occ = StringUtils.countMatches(str, "<");
            	int occ2 = StringUtils.countMatches(str, ">");
            	
            	int firstocc = str.indexOf("<");
            	int firstocc2 = str.indexOf(">");
            	
            	if (occ >= occ2 && firstocc <= firstocc2 && (lastSubStart != start || lastSubEnd != end))
            	{
            		
            		int z = start;
  		
            		if (nsb.substring(start + realIndexList.get((start - 1) % modRealIndex),
                			start + 1 + realIndexList.get((start - 1) % modRealIndex)).equals("<"))
                	{
                		nsb = nsb.replace(start + realIndexList.get((start - 1) % modRealIndex)
                				, end + realIndexList.get((end - 1) % modRealIndex) + 1
                				, "<" + tag + nsb.substring(start + realIndexList.get((start - 1) % modRealIndex)
                						, end + realIndexList.get((end - 1) % modRealIndex) + 1) + ">");
                	}
                	else
                		nsb = nsb.replace(start + realIndexList.get(start % modRealIndex)
            				, end + realIndexList.get((end - 1) % modRealIndex) + 1
            				, "<" + tag + nsb.substring(start + realIndexList.get(start % modRealIndex)
            						, end + realIndexList.get((end - 1) % modRealIndex) + 1) + ">");
         		
	            	for (int i = z; i < end; ++i)
	            		realIndexList.set(i % modRealIndex, realIndexList.get(i % modRealIndex) + 1 + lengthType);
	            	
	            	realIndexList.set(end % modRealIndex, realIndexList.get(end % modRealIndex) + 2 + lengthType);	
	            	
	            	for (int i = end + 1; i <= lastEnd; ++i)
	            		realIndexList.set(i % modRealIndex, realIndexList.get(i % modRealIndex) + 2 + lengthType);
	            	
	            	lastSubStart = start;
	            	lastSubEnd = end;
	            	
            	}
            	
            	
            }
		}
	
		nsb.delete(0, 1);
		
		try {
			nh.openOutputFile("testPrintLingPipe");
			nh.writeOutput(nsb.toString());
			nh.CloseOutputFile();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			nh.openOutputFile("textLingPipe");
			nh.writeOutput(sb.toString());
			nh.CloseOutputFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	
	public void printNestedTerms(Chunking chunking, StringBuilder sb)
	{
		NormHM nh = new NormHM();
		
		StringBuilder nsb = new StringBuilder();
		int realIndex = 0;
		int modRealIndex = 1000;
		ArrayList<Integer> realIndexList = new ArrayList<Integer>(Collections.nCopies(modRealIndex, 0));
		int lastStart = -1;
		int lastEnd = 0;
		
		
		realIndexList.set(0, 0);
		
		for (Chunk chunk: chunking.chunkSet())
		{
			int start = chunk.start();
            int end = chunk.end();
            String type = chunk.type();
            double score = chunk.score();
            String phrase = sb.substring(start,end);
            
            if (start < lastStart)
            {
            	String newChunk = nsb.substring(start + realIndexList.get(start % modRealIndex)
            			, lastEnd + realIndexList.get((lastEnd - 1) % modRealIndex));
          
            	for (int i = 0; i < newChunk.length(); ++i)
            	{
            		int subIndex = 0;
        		
            		if (newChunk.substring(i, i + 1).equals(">") || newChunk.substring(i, i + 1).equals("<"))
            		{
            			++subIndex;
            			continue;
            		}
            		
            		if (i + start - subIndex < lastEnd)
            			realIndexList.set((i + start - subIndex) % modRealIndex
            					, realIndexList.get((i + start - subIndex) % modRealIndex) - subIndex);
            		else
            			realIndexList.set((i + start - subIndex) % modRealIndex
            					, realIndexList.get((i + start - subIndex) % modRealIndex) - subIndex);
            	}
            	
            	int addIndex = 0;
         	
            	if (nsb.substring(start + realIndexList.get(start % modRealIndex) - 1
            			, start + realIndexList.get(start % modRealIndex)).equals("<"))
            		nsb = nsb.replace(start + realIndexList.get(start % modRealIndex)
            				, lastEnd + realIndexList.get((lastEnd - 1) % modRealIndex)
            				, sb.substring(start, end));	
            	else
            	{
            		nsb = nsb.replace(start + realIndexList.get(start % modRealIndex)
            				, lastEnd + realIndexList.get((lastEnd - 1) % modRealIndex)
            				, "<" + sb.substring(start, end));
            		addIndex = 1;
            	}
            	   	
            	for (int i = lastEnd; i < end; ++i)
            		realIndexList.set(i % modRealIndex, realIndexList.get(start % modRealIndex) + addIndex);
            	          	
            	realIndexList.set(end % modRealIndex, realIndexList.get((end - 1) % modRealIndex) + 1); 
     	
            	if (!nsb.substring(nsb.length() - 1).equals(">"))
            		nsb.append(">");	
	
            	lastStart = start;
                lastEnd = end;
            }
            else if (start == lastStart && end > lastEnd)
            {
            	if (nsb.substring(nsb.length() - 1).equals(">"))
            		nsb = nsb.deleteCharAt(nsb.length() - 1);
            	
            	nsb.append(sb.substring(lastEnd, end) + ">");
            	
            	realIndex = realIndexList.get((lastEnd - 1) % modRealIndex);
            	realIndexList.set(lastEnd % modRealIndex, realIndex);
            	
          	
            	for (int i = lastEnd + 1; i < end; ++i)
            		realIndexList.set(i % modRealIndex, realIndex);
            	
            	realIndexList.set(end % modRealIndex, realIndex + 1);
            	
            	lastStart = start;
                lastEnd = end;
            }
            else if (start > lastEnd)
            {
            	nsb.append(sb.substring(lastEnd, start));
            	nsb.append("<" + sb.substring(start, end) + ">");
            	
            	realIndex = realIndexList.get(lastEnd % modRealIndex);
            	
            	for (int i = lastEnd + 1; i < start; ++i)
            		realIndexList.set(i % modRealIndex, realIndex);
            	
            	
            	
            	for (int i = start; i < end; ++i)
            		realIndexList.set(i % modRealIndex, realIndex + 1);
            	
            	realIndexList.set(end % modRealIndex, realIndex + 2);
            	
            	lastStart = start;
                lastEnd = end;
            }
		}
		
		try {
			nh.openOutputFile("testPrintLingPipe");
			nh.writeOutput(nsb.toString());
			nh.CloseOutputFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			nh.openOutputFile("textLingPipe");
			nh.writeOutput(sb.toString());
			nh.CloseOutputFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public Chunking chunk(ExactDictionaryChunker chunker, String text)
	{
		 
		 NormHM nh = new NormHM();
		 Chunking chunking = chunker.chunk(text);
		 
		 try {
			nh.openOutputFile("lingpipe");
		
		 
			nh.writeOutput("\nChunker."
	                           + " All matches=" + chunker.returnAllMatches()
	                           + " Case sensitive=" + chunker.caseSensitive()
	                           + System.getProperty("line.separator"));
			
	       
	        for (Chunk chunk : chunking.chunkSet()) {
	            int start = chunk.start();
	            int end = chunk.end();
	            String type = chunk.type();
	            double score = chunk.score();
	            String phrase = text.substring(start,end);
	            nh.writeOutput("     phrase=|" + phrase + "|"
	                               + " start=" + start
	                               + " end=" + end
	                               + " type=" + type
	                               + " score=" + score
	                               + System.getProperty("line.separator"));
	        }
	        
	        nh.CloseOutputFile();
	        
		 } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 return chunking;
	}

	
	public void nestedTerms(HashMap<String, ArrayList<MyOntClass>> merge, HashMap<String, ArrayList<MyOntClass>> nestedWithSyn)
	{
		NormHM nh = new NormHM();	
		
		MapDictionary<String> dictionary = new MapDictionary<String>();
		MapDictionary<String> dictionaryWithSyn = new MapDictionary<String>();
			
		for (Iterator<Map.Entry<String, ArrayList<MyOntClass>>> k = merge.entrySet().iterator(); k.hasNext();)
		{
			Map.Entry<String, ArrayList<MyOntClass>> e = k.next();	
			
			ArrayList<String> SemTypeList = new ArrayList<String>();
			
			for (MyOntClass cls: e.getValue())
			{
				if (!SemTypeList.contains(cls.getSemType().getAcronym()))
					SemTypeList.add(cls.getSemType().getAcronym());
				
				for (String s: cls.getSynList())
				{
					DictionaryEntry<String> de = new DictionaryEntry<String>(s, e.getKey(), CHUNK_SCORE);
					dictionaryWithSyn.addEntry(de);
				}
			}
			
			DictionaryEntry<String> de = new DictionaryEntry<String>(e.getKey(), e.getKey(), CHUNK_SCORE);
			dictionary.addEntry(de);
			dictionaryWithSyn.addEntry(de);
		}
		
		ExactDictionaryChunker dictionaryChunkerTF
	    		= new ExactDictionaryChunker(dictionary,
	                                         IndoEuropeanTokenizerFactory.INSTANCE,
	                                         true,false);
		
		ExactDictionaryChunker dictionaryWithSynChunkerTF
        		= new ExactDictionaryChunker(dictionaryWithSyn,
                                     IndoEuropeanTokenizerFactory.INSTANCE,
                                     true,false);
   		
	        
		    for (Iterator<Map.Entry<String, ArrayList<MyOntClass>>> k = merge.entrySet().iterator(); k.hasNext();)
		    {
		    	Map.Entry<String, ArrayList<MyOntClass>> e = k.next();
		    	
		    	String text = "";
		    	
		    	text += e.getKey();
		    	text += System.getProperty("line.separator");
		    	
		    	ArrayList<String> listNested = chunkNested(dictionaryChunkerTF, text, e.getKey());
		    	ArrayList<String> listNestedWithSyn = chunkNested(dictionaryWithSynChunkerTF, text, e.getKey());

		    	for (MyOntClass cls: e.getValue())
		    		cls.setNestedList(listNested);
		    	
		    	ArrayList<MyOntClass> newClassList = new ArrayList<MyOntClass>();
		    	
		    	for (MyOntClass cls: e.getValue())
		    	{
		    		MyOntClass newClass = new MyOntClass(cls);
		    		newClass.setNestedList(listNestedWithSyn);
		    		newClassList.add(newClass);		    		
		    	}
		    	nestedWithSyn.put(e.getKey(), newClassList);
		    }
	}
	
	public void searchSharedAndSynTerms(ArrayList<HashMap<String, MyOntClass>> allClasses
			, HashMap<String, Integer> filesIndex
			, HashMap<String, ArrayList<MyOntClass>> sharedTerms
			, HashMap<String, ArrayList<MyOntClass>> synTerms)
	{
		NormHM nh = new NormHM();
		
		MapDictionary<String> dictionary = new MapDictionary<String>();
		
		for (int i = 0; i < allClasses.size(); ++i)
		{
			for (Iterator<Map.Entry<String, MyOntClass>> k = allClasses.get(i).entrySet().iterator(); k.hasNext();)
			{
				Map.Entry<String, MyOntClass> e = k.next();
				
				DictionaryEntry<String> de = new DictionaryEntry<String>(e.getValue().getLabel(), e.getValue().getFileName(), CHUNK_SCORE);
				dictionary.addEntry(de);
			}
		}
		
		ExactDictionaryChunker dictionaryChunkerTF
        = new ExactDictionaryChunker(dictionary,
                                     IndoEuropeanTokenizerFactory.INSTANCE,
                                     true, false);
		
		for (int i = 0; i < allClasses.size(); ++i)
		{
			for (Iterator<Map.Entry<String, MyOntClass>>k = allClasses.get(i).entrySet().iterator(); k.hasNext();)
			{
				Map.Entry<String, MyOntClass> e = k.next();
				
				String text = e.getValue().getLabel();
				
				ArrayList<String> listFiles = chunkSharedTerms(dictionaryChunkerTF
						, text
						, nh);
				
				ArrayList<MyOntClass> listCls = new ArrayList<MyOntClass>();
				
				for (String file: listFiles)
				{
					int j = filesIndex.get(file);
					
					if (allClasses.get(j).containsKey(text))
					{
						MyOntClass cls = allClasses.get(j).get(e.getKey());
						
						if (!listCls.contains(cls))
							listCls.add(cls);
					}
				}
				
				if (listCls.size() > 1)
					sharedTerms.put(text, listCls);
				
			
				for (String s: e.getValue().getSynList())
				{
					
					ArrayList<MyOntClass> synList = new ArrayList<MyOntClass>();
					ArrayList<String> listSynFiles = chunkSynTerms(dictionaryChunkerTF, s, nh);
				
					synList.add(e.getValue());
							
					for (String file: listSynFiles)
					{
						int j = filesIndex.get(file);
						
						if (allClasses.get(j).containsKey(s))
						{
							MyOntClass cls = allClasses.get(j).get(s);
							
							if (!synList.contains(cls))
								synList.add(cls);
						}
					}
								
					if (synTerms.containsKey(e.getValue().getLabel()))
					{
						ArrayList<MyOntClass> newSynList = synTerms.get(e.getValue().getLabel());
						
						for (MyOntClass cls: synList)
			        	{
							if (!newSynList.contains(cls))
								newSynList.add(cls);
						}						
					}
					else
					{
						if (synList.size() > 1)
							synTerms.put(e.getValue().getLabel(), synList);
					}
				}
			}
		}		
	}
	
	public ArrayList<String> chunkSharedTerms(ExactDictionaryChunker chunker
									, String text
									, NormHM nh)
	{
		
		ArrayList<String> listFiles = new ArrayList<String>();
	    Chunking chunking = chunker.chunk(text);
	    
	    for (Chunk chunk : chunking.chunkSet()) 
	    {
	        int start = chunk.start();
	        int end = chunk.end();
	        String type = chunk.type();
	        double score = chunk.score();
	        String phrase = text.substring(start,end);	       
	        
	        if (phrase.equals(text) && !listFiles.contains(type))
	        	listFiles.add(type);
	    }
	 
		return listFiles;
	}

	public ArrayList<String> chunkSynTerms(ExactDictionaryChunker chunker
			, String text
			, NormHM nh)
	{

		ArrayList<String> listFiles = new ArrayList<String>();
		Chunking chunking = chunker.chunk(text);
		
		for (Chunk chunk : chunking.chunkSet()) 
		{
			int start = chunk.start();
			int end = chunk.end();
			String type = chunk.type();
			double score = chunk.score();
			String phrase = text.substring(start,end);	  
			
			if (phrase.equals(text) && !listFiles.contains(type))
				listFiles.add(type);
		}
		
		return listFiles;
	}
	
	public ArrayList<String> chunkNested(ExactDictionaryChunker chunker
			, String text
			, String label)
	{	
		
		ArrayList<String> listNested = new ArrayList<String>();
		
	    Chunking chunking = chunker.chunk(text);
	   
	    for (Chunk chunk : chunking.chunkSet()) 
	    {
	        int start = chunk.start();
	        int end = chunk.end();
	        String type = chunk.type();
	        double score = chunk.score();
	        String phrase = text.substring(start,end);
	     
	        if (!listNested.contains(type) && !type.equals(label) && !phrase.equals(label))
	        	listNested.add(type);
	    }
	
		return listNested;
	}

	public void ontStats(ArrayList<HashMap<String, MyOntClass>> allClasses
			, ArrayList<String> filename
			, HashMap<String, ArrayList<MyOntClass>> sharedTerms
			, HashMap<String, ArrayList<MyOntClass>> synTerms
			, HashMap<String, ArrayList<MyOntClass>> merge)
	{
		String output = "";
		int total = 0;
		NormHM nh = new NormHM();
		
		HashMap<String, Integer> nbrTotalId = new HashMap<String, Integer>();
		
		for (int i = 0; i < allClasses.size(); ++i)
		{
			output += filename.get(i).split("/")[filename.get(i).split("/").length - 1] + " :" + allClasses.get(i).size() + " terms"
					+ System.getProperty("line.separator");
			output += nh.ontStatFile(allClasses.get(i), nbrTotalId);
			
			total += allClasses.get(i).size();
		}
		
		output += System.getProperty("line.separator");
		output += "The repartition of the ids across all the ontologies:";
		output += System.getProperty("line.separator");
		
		nbrTotalId = (HashMap<String, Integer>) MapUtil.sortByValue(nbrTotalId);
		for (Iterator<Map.Entry<String, Integer>> k = nbrTotalId.entrySet().iterator(); k.hasNext();)
		{
			
			Map.Entry<String, Integer> e = k.next();
			float pourc = ((float) e.getValue() / total) * 100;
			
			output += "\t" + e.getKey() + ": " + e.getValue() + "\t\t" + pourc + "%"
					+ System.getProperty("line.separator");
		}
		
		output += System.getProperty("line.separator");
		
		output += nh.ontStatSharedTerms(sharedTerms);
		output += nh.ontStatSynTerms(synTerms);
		output += nh.ontStatMerge(merge);		
				
		try {
			
			nh.openOutputFile("LingPipeStats");
			nh.writeOutput(output);
			nh.CloseOutputFile();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}
	
	
}
