package ont;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import monq.jfa.CompileDfaException;
import monq.jfa.Dfa;
import monq.jfa.DfaRun;
import monq.jfa.FaAction;
import monq.jfa.Nfa;
import monq.jfa.ReSyntaxException;
import monq.jfa.ReaderCharSource;
import monq.jfa.actions.Printf;


import org.apache.jena.ontology.OntModel;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


public class Main {
	
	static Logger log = Logger.getLogger(Main.class);
	
	
	/**
	 *  Get the RDF files that are in the directory 
	 * @param dir the directory where we look for the files
	 * @return  an arraylist of the files
	 */
	public static ArrayList<String> getOntFiles(String dir) {
		
		log.info("Get RDF file names");
		
		ArrayList<String> listFiles = new ArrayList<String>();
		File curDir = new File(dir);
      
		File[] filesList = curDir.listFiles();
        
		for(File f : filesList)
		{			
            if(f.isFile() && f.getName().endsWith("rdf"))
            	listFiles.add(f.getAbsolutePath().replaceAll("/./", "/"));
        }
		
        return listFiles;
	}
	
	public static void main(String[] args) {
		
		
		ArrayList<HashMap<String, MyOntClass>> allClasses = new ArrayList<HashMap<String, MyOntClass>>();
		NormHM nh = new NormHM();
		
		ArrayList<String> files = getOntFiles(".");
		HashMap<String, Integer> filesIndex = new HashMap<String, Integer>();
	
		ReadOnt r = new ReadOnt();
		
//		r.createAllDatasets(files);
		
		int j = 0;
		for (String inputFile: files)
		{
			HashMap<String, MyOntClass> classes = new HashMap<String, MyOntClass>();
			r.readOnt(inputFile, classes);
			allClasses.add(classes);
			filesIndex.put(inputFile, j);
			++j;
		}

		log.info("begin stop words");
		for (int i = 0; i < allClasses.size(); ++i)
		{
			HashMap<String, MyOntClass> newClass = nh.delStopWords(allClasses.get(i), files.get(i));
			allClasses.set(i, newClass);
		}	
		log.info("end stop words");
		
		HashMap<String, SemType> allSems = new HashMap<String, SemType>();
		nh.createSemType(allSems);
					
		log.info("begin lingpipe");
		NormLingPipe nl = new NormLingPipe();
		
		HashMap<String, ArrayList<MyOntClass>> sharedTerms = new HashMap<String, ArrayList<MyOntClass>>();
		HashMap<String, ArrayList<MyOntClass>> synTerms = new HashMap<String, ArrayList<MyOntClass>>();
		HashMap<String, ArrayList<MyOntClass>> merge = new HashMap<String, ArrayList<MyOntClass>>();
		HashMap<String, ArrayList<MyOntClass>> nestedWithSyn = new HashMap<String, ArrayList<MyOntClass>>();
		nl.searchSharedAndSynTerms(allClasses, filesIndex, sharedTerms, synTerms);
		nh.mergeTerms(allClasses, merge, sharedTerms);
		
		log.info("begin sem type");
		nh.addSemanticType(merge, allSems);	
		log.info("end sem typ");
		
		nl.nestedTerms(merge, nestedWithSyn);
		
		nl.ontStats(allClasses, files, sharedTerms, synTerms, merge);
	//	nh.ontStats(allClasses, files, sharedTerms, synTerms, merge);

		
		SerializeObject so = new SerializeObject();
//		HashMap<String, ArrayList<MyOntClass>> merge2 = so.DeserializeMergeHashMap("merge");
		nl.nestedTermsTest(merge);
		
		so.SerializeMergeHashMap(merge, "merge");
		
		log.info("end lingpipe");

//		r.listAllClasses(m3, classes);
	}

}
