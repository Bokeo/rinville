package ont;


import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import org.apache.jena.ontology.AnnotationProperty;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.tdb.TDBFactory;
import org.apache.jena.util.FileManager;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.VCARD;
import org.apache.log4j.Logger;


/**
 * ReadOnt is a class that read an ontology with the library jena
 * This class takes information of the ontology and stores them
 * in an hashmap. This hashmap has the label of an ontology as key and
 * @class MyOntClass as value.
 * @author alerin
 *
 */
public class ReadOnt {
	
	static Logger log = Logger.getLogger(ReadOnt.class);
	
	private BufferedWriter bw = null;
	private FileWriter fw = null; 
	private Dataset dataset = null;
	private Model base = null;
	private SerializeObject so = null;
	
	
	public ReadOnt()
	{
		so = new SerializeObject();
	}
	
	/**
	 * readOnt sees if we already have the serialized hashmap else we will read
	 * the RDF file and add the information in the hasmap
	 * @param fileName the filename of the ontology we want to read
	 * @param classes the hashmap that stores the informaiton 
	 */
	public void readOnt(String fileName, HashMap<String, MyOntClass> classes)
	{	
		log.info("begin " + fileName);
        
	    // read the RDF/XML file
		String directory =  fileName.split("/")[fileName.split("/").length - 1];
		directory = directory.split("\\.")[0];
		
		File f = new File("hashmap/" + directory + ".ser");
		
		if (f.isFile())
		{
			classes.putAll(so.DeserializeHashMap(directory));
					
			if (classes != null)
			{	
				log.info("end " + fileName);
				return;
			}
		}
		
		OntModel m = readRDF(fileName);
		addClsInHM(m, classes);
		closeRDF(m);
		so.SerializeHashMap(classes, directory);
		
		log.info("end " + fileName);
	}
	
	
	public void createAllDatasets(ArrayList<String> files)
	{	
		for (String filename: files)
		{
			String directory = "dataset/" + filename.split("/")[filename.split("/").length - 1];
			directory = directory.split("\\.")[0];
			
			File f = new File(directory);
				
			if (!f.isDirectory())
				createTDB(filename);		
		}
	}
	
	/**
	 * createTDB creates the TDB dataset of the file
	 * @param fileName the path of the file
	 */
	public void createTDB(String fileName)
	{
		log.info("Create dataset for " + fileName);
		
		String directory = "dataset/" + fileName.split("/")[fileName.split("/").length - 1];
		Dataset dataset = TDBFactory.createDataset(directory.split("\\.")[0]);
		
		Model tdb = dataset.getDefaultModel();
		
		// read the input file
		String source = fileName;
		FileManager.get().readModel(tdb, source);
			
		tdb.close();
		dataset.close();
		
		log.info("Dataset done");
	}
	
	/**
	 * readRDF creates a model of the ontology with jena
	 * if there is not a tdb dataset for the ontology @method readRDF call @method createTDB
	 * @param fileName the path of the file
	 * @return the model of the ontology
	 */
	public OntModel readRDF(String fileName)
	{
		log.info("begin rdf " + fileName);
				        
	    // read the RDF/XML file
		String directory = "dataset/" + fileName.split("/")[fileName.split("/").length - 1];
		directory = directory.split("\\.")[0];
		
		File f = new File(directory);
			
		if (!f.isDirectory())
			createTDB(fileName);
		
		dataset = TDBFactory.createDataset(directory);
		base = dataset.getDefaultModel();
		OntModel model = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM, base);
		
	    log.info("end rdf " + fileName);
	    
		return model;
	}
	
	protected String ListAllProperties(OntClass cls)
	{
		String result = "";
		
		 // list all properties of the classes
    	ExtendedIterator<OntProperty> propIter = cls.listDeclaredProperties();
    	
    	while (propIter.hasNext()) 
    	{
    		OntProperty property = (OntProperty) propIter.next(); 
    		
    			result += property.getLocalName() + " " + cls.getPropertyValue(property)
    					+ " " + property.getLabel(property.getURI())
    				+ System.getProperty("line.separator");
    			
    	} 
    	
    	StmtIterator sts = cls.listProperties();
    	
    	while (sts.hasNext())
    	{
    		Statement st = (Statement) sts.next();
    	
			String syn = "";
			syn = st.getPredicate().getLocalName().toLowerCase() + " "
			+ st.getObject().toString() + " " + st.toString()
			+ System.getProperty("line.separator");;
			result += syn;       	
    	}
		
		return result;
	}
	
	public void listSynAndAncesProperties(OntClass cls, MyOntClass mycls, OntModel model)
	{
		ArrayList<String> result = new ArrayList<String>();
		
		 // list all properties of the classes
	   	ExtendedIterator<OntProperty> propIter = cls.listDeclaredProperties();
	   	
	   	while (propIter.hasNext()) 
	   	{
	   		OntProperty property = (OntProperty) propIter.next();   	
	   		
	   		if (property.getLocalName().toLowerCase().contains("synonym") 
	   			&& cls.getPropertyValue(property) != null)
	   		{
	   			
	   			for (RDFNode rn: cls.listPropertyValues(property).toList())
	   			{
	   				String syn = "";
	   				syn = rn.toString().toLowerCase();
	       			result.add(syn);
	   			}
	   		}    		
	   	}
	   	
	   	StmtIterator sts = cls.listProperties();
	   	
	   	while (sts.hasNext())
	   	{
	   		Statement st = (Statement) sts.next();
	   	
	   		if (st.getPredicate().getLocalName().contains("synonym"))
	   		{
	   			String syn = "";   			
	   			syn = st.getLiteral().getString().toLowerCase();
	   			result.add(syn);
	   		}
	   		
	   		if (st.getPredicate().getLocalName().equals("subClassOf"))
        	{
        		String ancesCls = st.getObject().toString();
        		Individual ind = model.getIndividual(ancesCls);
        		
        		if (ind != null)
        		{
        			Statement labelSt = ind.getProperty(RDFS.label);
        			if (labelSt != null)
        			{
	        			String ances =  labelSt.getString().toLowerCase();
	        			mycls.addAncestor(ances);
        			}
        		}  	  
        	}
	   		
	   		if (st.getPredicate().getLocalName().toLowerCase().contains("obonamespace"))
	   		{
	   			String namespace = st.getObject().toString();	
	   			mycls.setOboNamespace(namespace);
	   		}
	   	}
	   	
	   	mycls.setSynList(result);
	}
	
	/** 
	 * addClsInHM list all classes of the ontology model and take the synonyms to
	 * put them in the hashmap
	 * @param model the model of the ontology
	 * @param myClasses the hashmap of the ontology
	 */
	public void addClsInHM(OntModel model, HashMap<String, MyOntClass> myClasses)
	{
		log.info("begin ont");
				
		List<AnnotationProperty> ei = model.listAnnotationProperties().toList();		
		AnnotationProperty defProperty = null;
		
		for (AnnotationProperty ap: ei)
		{
			if ((ap.getLocalName() != null && ap.getLocalName().contains("definition")) 
					|| (ap.getPropertyValue(RDFS.label) != null
					&& ap.getPropertyValue(RDFS.label).toString().contains("definition")))
				defProperty = ap;			
		}	
				
		List<OntClass> lcls = model.listClasses().toList();
		
		log.info(lcls.size());
	
		for (OntClass cls: lcls)
		{
        	
// 			id and label
        	String idCls = cls.getLocalName();
        	if (cls == null || idCls == null || cls.getProperty(RDFS.label) == null 
        		|| cls.getProperty(RDFS.label).getString() == null)
        		continue;
        	
        	String labelCls = cls.getProperty(RDFS.label).getString().toLowerCase();
        	
        	if (!myClasses.containsKey(labelCls))
        	{
        		MyOntClass myCls = new MyOntClass(labelCls);
        		myCls.addIdInList(idCls);
        		myCls.setId(idCls);
        		myClasses.put(labelCls, myCls);
        		listSynAndAncesProperties(cls, myCls, model);
        		
        		if (defProperty != null && cls.getPropertyValue(defProperty) != null)
        			myCls.setDefinition(cls.getPropertyValue(defProperty).toString());
        		
        	}
        	else
        	{
        		MyOntClass myCls = myClasses.get(labelCls);
        		myCls.addIdInList(idCls);
        		myCls.setId(idCls);
        		listSynAndAncesProperties(cls, myCls, model);
        		if (defProperty != null && cls.getPropertyValue(defProperty) != null)
        			myCls.setDefinition(cls.getPropertyValue(defProperty).toString());
        	}   
		}
	
		log.info("end Ont");
	}

	public void closeRDF(OntModel model)
	{
		model.close();
		base.close();
		dataset.close();
	}
	
	public void openOutputFile() throws IOException
	{
		File file = new File("./output.txt");
		log.info("Create Output file");
		
		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		fw = new FileWriter(file.getAbsoluteFile());
	    bw = new BufferedWriter(fw);
	    
	}
	
	public void CloseOutputFile() throws IOException
	{
		bw.close();
		fw.close();
		log.info("Done");
	}
	
	public void writeOutput(String output) throws IOException
	{
		bw.write(output);
	}
	
}
