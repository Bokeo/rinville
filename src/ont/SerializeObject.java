package ont;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import ont.MyOntClass;

import org.apache.log4j.Logger;


public class SerializeObject {
	
	static Logger log = Logger.getLogger(SerializeObject.class);
	
	public SerializeObject()
	{
		
	}
	
	public void SerializeHashMap(HashMap<String, MyOntClass> hmap, String fileName)
	{
		try {
			FileOutputStream fos = new FileOutputStream("hashmap/" + fileName + ".ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(hmap);
			oos.close();
			fos.close();
			
			log.info("Serialized HashMap data is saved in hashmap/" + fileName + ".ser");
		}
		catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public HashMap<String, MyOntClass> DeserializeHashMap(String fileName)
	{
		HashMap<String, MyOntClass> map = null;
	    
		try {
			FileInputStream fis = new FileInputStream("hashmap/" + fileName + ".ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			map = (HashMap) ois.readObject();
			ois.close();
			fis.close();
	         
	    }
		catch(IOException ioe) {
			ioe.printStackTrace();
			return null;
		}
		catch(ClassNotFoundException c) {
			log.info("Class not found");
			c.printStackTrace();
			return null;
		}
		
		log.info("Deserialized HashMap");
		
		return map;
	}
	
	public void SerializeMergeHashMap(HashMap<String, ArrayList<MyOntClass>> hmap, String fileName)
	{
		try {
			FileOutputStream fos = new FileOutputStream("./" + fileName + ".ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(hmap);
			oos.close();
			fos.close();
			
			log.info("Serialized HashMap data is saved in ./" + fileName + ".ser");
		}
		catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public HashMap<String, ArrayList<MyOntClass>> DeserializeMergeHashMap(String fileName)
	{
		HashMap<String, ArrayList<MyOntClass>> map = null;
	    
		try {
			FileInputStream fis = new FileInputStream("./" + fileName + ".ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			map = (HashMap) ois.readObject();
			ois.close();
			fis.close();
	         
	    }
		catch(IOException ioe) {
			ioe.printStackTrace();
			return null;
		}
		catch(ClassNotFoundException c) {
			log.info("Class not found");
			c.printStackTrace();
			return null;
		}
		
		log.info("Deserialized HashMap");
		
		return map;
	}


}
