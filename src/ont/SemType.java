package ont;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SemType implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5667821379444214268L;
	
	private String label;
	private String acronym;
	private SemType parent = null;
	private ArrayList<SemType> children = new ArrayList<SemType>();
	
	public SemType(String label, String acronym)
	{
		this.label = label;
		this.acronym = acronym;
	}
	
	public SemType(String label, SemType parent)
	{
		this.label = label;
		this.setParent(parent);
	}
	
	public SemType(String label, SemType parent, ArrayList<SemType> children)
	{
		this.label = label;
		this.setParent(parent);
		this.setChildren(children);
	}
	
	
	
	public boolean isAncestor(SemType sem)
	{
		SemType anc = this.parent;
		
		while (anc != null)
		{
			if (anc.equals(sem))
				return true;
			
			anc = anc.parent;
		}
		
		return false;
	}
	
	public boolean isChildren(SemType sem)
	{
		for (SemType child: this.children)
		{
			if (child.equals(sem))
				return true;
			else
				isChildren(child);
		}
		
		return false;
	}
	
	public String getLabel()
	{
		return label;
	}
	
	public void setLabel(String label)
	{
		this.label = label;
	}
	
	public String getAcronym()
	{
		return acronym;
	}
	
	public void setAcronym(String acronym)
	{
		this.acronym = acronym;
	}
	
	public void setParent(SemType parent)
	{
		this.parent = parent;
		parent.addChild(this);
	}
	
	public SemType getParent()
	{
		return parent;
	}
	
	public void setChildren(ArrayList<SemType> children)
	{
		this.children = children;
		
		for (SemType s: children)
			s.parent = this;
	}
	
	public ArrayList<SemType> getChildren()
	{
		return children;
	}
	
	public void addChild(SemType child)
	{
		if (!this.children.contains(child))
		{
			this.children.add(child);
			child.parent = this;
		}
	}
	
	public void addChildren(ArrayList<SemType> children)
	{
		for (SemType s: children)
		{
			if (!this.children.contains(s))
			{
				this.children.add(s);
				s.parent = this;
			}
		}
	}
}
