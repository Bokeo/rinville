package ont;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * MyOntClass is a class that store the label, the different ids, synonyms
 * and nested terms of an ontology term.
 * @author alerin
 *
 */
public class MyOntClass implements Serializable {
	
	private static final long serialVersionUID = 5586958769377611928L;
	
	private String label;
	private String rlabel;
	private String id;
	private ArrayList<String> idList = new ArrayList<String>();
	private ArrayList<String> synList = new ArrayList<String>();
	private ArrayList<String> nestedList = new ArrayList<String>();
	private ArrayList<String> ancestorList = new ArrayList<String>();
	private String definition = null;
	private String fileName;
	private String oboNamespace;
	private SemType semType;
	
	
	public MyOntClass(String label)
	{
		this.setLabel(label);
		this.setRealLabel(label);
	}
	
	public MyOntClass(MyOntClass another)
	{
		this.label = another.label;
		this.rlabel = another.rlabel;
		this.id = another.id;
		this.idList = new ArrayList<String>(another.idList);
		this.synList = new ArrayList<String>(another.synList);
		this.nestedList = new ArrayList<String>(another.nestedList);
		this.ancestorList = new ArrayList<String>(another.ancestorList);
		this.definition = another.definition;
		this.fileName = another.fileName;
		this.oboNamespace = another.oboNamespace;
		this.semType = another.semType;
		
	}

	public void addId(String newId)
	{
		if (!idList.contains(newId))
			idList.add(newId);
	}
	
	/**
	 * Adds a new id in the list
	 * @param id the id that we want to add in the list
	 */
	public void addIdInList(String id)
	{
		if (!idList.contains(id))
			idList.add(id);
	}
	
	/**
	 * Add a list of ids in the list
	 * @param ids the list of ids that we want to add
	 */
	public void addIdsInList(ArrayList<String> ids)
	{
		for (String id: ids)
		{
			if (!idList.contains(id))
				idList.add(id);
		}
	}
	
	public ArrayList<String> getIdList()
	{
		return idList;
	}
	
	/**
	 * Adds a new synonym in the list
	 * @param newSyn the synonym that we want to add in the list
	 */
	public void addSyn(String newSyn)
	{
		if (!synList.contains(newSyn))
			synList.add(newSyn);
	}
	
	/**
	 * Add a list of synonyms in the list
	 * @param syn the list of synonyms that we want to add
	 */
	public void addSynInList(ArrayList<String> syn)
	{
		for (String s: syn)
		{
			if (!synList.contains(s))
				synList.add(s);
		}
	}
	
	public ArrayList<String> getSynList()
	{
		return synList;
	}
	
	public void setSynList(ArrayList<String> syns)
	{
		this.synList = syns;
	}
	
	public void addListInNestedList(ArrayList<String> list)
	{
		for (String s: list)
		{
			if (!nestedList.contains(s))
				nestedList.add(s);
		}
	}
	
	public ArrayList<String> getNestedList()
	{
		return nestedList;
	}
	
	public void setNestedList(ArrayList<String> nestedList)
	{
		this.nestedList = nestedList;
	}

	/**
	 * Adds a new nested term in the list
	 * @param word the nested term that we want to add in the list
	 */
	public void addNestedinList(String word)
	{	
		if (!nestedList.contains(word))
			nestedList.add(word);
		
	}

	public void addAncestor(String newAnces)
	{
		if (!ancestorList.contains(newAnces))
			ancestorList.add(newAnces);
	}
	
	
	public void addAncesInList(ArrayList<String> ances)
	{
		for (String a: ances)
		{
			if (!ancestorList.contains(a))
				ancestorList.add(a);
		}
	}
	
	public ArrayList<String> getAncesList()
	{
		return ancestorList;
	}
	
	public void setAncList(ArrayList<String> ancList)
	{
		this.ancestorList = ancList;
	}
	
	public String getLabel()
	{
		return label;
	}

	public void setLabel(String label) 
	{
		this.label = label;
	}
	
	public String getId()
	{
		return id;
	}

	public void setId(String id) 
	{
		this.id = id;
	}
	
	public String getRealLabel()
	{
		return rlabel;
	}

	public void setRealLabel(String rlabel) 
	{
		this.rlabel = rlabel;
	}
	
	public String getDefinition()
	{
		return definition;
	}
	
	public void setDefinition(String definition)
	{
		this.definition = definition;
	}
	
	public String getFileName()
	{
		return fileName;
	}
	
	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}
	
	public void setIdList(ArrayList<String> idList) {
		this.idList = idList;
	}

	public void setAncestorList(ArrayList<String> ancestorList) {
		this.ancestorList = ancestorList;
	}
	
	public SemType getSemType()
	{
		return semType;
	}

	public void setSemType(SemType semType)
	{
		this.semType = semType;
	}
	
	public String getOboNamespace()
	{
		return oboNamespace;
	}
	
	public void setOboNamespace(String oboNamespace)
	{
		this.oboNamespace = oboNamespace;
	}
}
